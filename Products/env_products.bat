@REM ============================================
@REM Author: Artem A. Zhidkov
@REM Date:   Dec 8, 2014
@REM ============================================


@REM ============================================
@REM ==  Avoid duplicated calling this file  ====
@IF "%DEFINE_ENV_PRODUCTS%" == "1" GOTO END
@REM ============================================

@REM ============================================
@REM ==  CMake  =================================
@SET CMAKE_ROOT_DIR=%PRODUCTS_ROOT_DIR%\cmake-3.1.3
@SET PATH=%CMAKE_ROOT_DIR%\bin;%PATH%
@REM ============================================

@REM ============================================
@REM ==  Boost  =================================
@SET BOOST_ROOT=%PRODUCTS_ROOT_DIR%\boost-1.57.0
@SET BOOST_INCLUDEDIR=%BOOST_ROOT%\include\boost-1_57
@SET BOOST_LIBRARYDIR=%BOOST_ROOT%\lib
@SET PATH=%BOOST_ROOT%\lib;%PATH%
@REM ============================================

@REM ============================================
@REM ==  Eigen  =================================
@SET EIGEN_ROOT_DIR=%PRODUCTS_ROOT_DIR%\eigen-3.2.4
@SET EIGEN_INCLUDE_DIR=%EIGEN_ROOT_DIR%\include\eigen3
@REM ============================================

@REM ============================================
@REM ==  Doxygen  ===============================
@SET DOXYGEN_ROOT_DIR=%PRODUCTS_ROOT_DIR%\doxygen-1.8.9.1\bin
@SET PATH=%DOXYGEN_ROOT_DIR%;%PATH%
@REM ============================================

@REM ============================================
@REM ==  Graphviz  ==============================
@SET GRAPHVIZ_ROOT_DIR=%PRODUCTS_ROOT_DIR%\graphviz-2.38.0\bin
@SET PATH=%GRAPHVIZ_ROOT_DIR%;%PATH%
@REM ============================================

@REM ============================================
@REM ==  Avoid duplicated calling this file  ====
@SET DEFINE_ENV_PRODUCTS=1
@REM ============================================
:END