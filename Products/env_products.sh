# ============================================
# Author: Artem A. Zhidkov
# Date:   Jan 2, 2015
# ============================================


# ============================================
# ==  Avoid duplicated calling this file  ====
if [ "${DEFINE_ENV_PRODUCTS}" != "1" ]; then
# ============================================

# ============================================
# ==  CMake  =================================
export CMAKE_ROOT_DIR=${PRODUCTS_ROOT_DIR}/cmake-3.1.3
export PATH=${CMAKE_ROOT_DIR}/bin:${PATH}
# ============================================

# ============================================
# ==  Boost  =================================
export BOOST_ROOT=${PRODUCTS_ROOT_DIR}/boost-1.57.0
export BOOST_INCLUDEDIR=${BOOST_ROOT}/include/boost-1_57
export BOOST_LIBRARYDIR=${BOOST_ROOT}/lib
export LD_LIBRARY_PATH=${BOOST_LIBRARYDIR}:${LD_LIBRARY_PATH}
# ============================================

# ============================================
# ==  Eigen  =================================
export EIGEN_ROOT_DIR=${PRODUCTS_ROOT_DIR}/eigen-3.2.4
export EIGEN_INCLUDE_DIR=${EIGEN_ROOT_DIR}/include/eigen3
# ============================================

# ============================================
# ==  Doxygen  ===============================
export DOXYGEN_ROOT_DIR=${PRODUCTS_ROOT_DIR}/doxygen-1.8.9.1/bin
export PATH=${DOXYGEN_ROOT_DIR}:${PATH}
# ============================================

# ============================================
# ==  Graphviz  ==============================
export GRAPHVIZ_ROOT_DIR=${PRODUCTS_ROOT_DIR}/graphviz-2.38.0/bin
export PATH=${GRAPHVIZ_ROOT_DIR}:${PATH}
# ============================================

# ============================================
# ==  Avoid duplicated calling this file  ====
export DEFINE_ENV_PRODUCTS=1
# ============================================
fi
