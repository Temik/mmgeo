@echo off

set ROOT_PATH=%~dp0
set PRODUCTS_ROOT_DIR=%ROOT_PATH%..\Products

call %PRODUCTS_ROOT_DIR%\env_products.bat
call %ROOT_PATH%env_RRC.bat

set RRC_BUILD_DOC=TRUE

mkdir %RRC_BUILD_DIR%
cd %RRC_BUILD_DIR%
cmake.exe -G "Visual Studio 12 2013 Win64" -DCMAKE_BUILD_TYPE=Debug -DCMAKE_INSTALL_PREFIX=%RRC_ROOT_DIR% %RRC_SRC_DIR%
cd %ROOT_PATH%

start "" "%VS120COMNTOOLS%\..\IDE\devenv.exe" %RRC_BUILD_DIR%\RRC.sln
