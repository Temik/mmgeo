# ============================================
# Author: Artem A. Zhidkov
# Date:   Jan 2, 2015
# ============================================

# ============================================
# ==  RRC directories  =======================
export RRC_ROOT_DIR=${ROOT_PATH}
export RRC_SRC_DIR=${RRC_ROOT_DIR}/RRC_SRC
export RRC_DOC_DIR=${RRC_ROOT_DIR}/RRC_DOC
export RRC_BUILD_DIR=${RRC_ROOT_DIR}/RRC_BUILD
export RRC_INSTALL_DIR=${RRC_ROOT_DIR}/RRC_INSTALL
# ============================================
