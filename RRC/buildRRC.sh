#!/bin/sh

ROOT_PATH=$(pwd)
PRODUCTS_ROOT_DIR=${ROOT_PATH}/../Products

. ${PRODUCTS_ROOT_DIR}/env_products.sh
. ${ROOT_PATH}/env_RRC.sh

export RRC_BUILD_DOC=TRUE

mkdir -p ${RRC_BUILD_DIR}
cd ${RRC_BUILD_DIR}
cmake -G "Eclipse CDT4 - Unix Makefiles" -DCMAKE_BUILD_TYPE=Debug -DCMAKE_INSTALL_PREFIX=${RRC_INSTALL_DIR} ${RRC_SRC_DIR}
cd ${ROOT_PATH}

eclipse -Dosgi.locking=none &
