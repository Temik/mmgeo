@REM ============================================
@REM Author: Artem A. Zhidkov
@REM Date:   Dec 8, 2014
@REM ============================================

@REM ============================================
@REM ==  RRC directories  =======================
@SET RRC_ROOT_DIR=%ROOT_PATH%
@SET RRC_SRC_DIR=%RRC_ROOT_DIR%RRC_SRC
@SET RRC_DOC_DIR=%RRC_ROOT_DIR%RRC_DOC
@SET RRC_BUILD_DIR=%RRC_ROOT_DIR%RRC_BUILD
@SET RRC_INSTALL_DIR=%RRC_ROOT_DIR%RRC_INSTALL
@REM ============================================

