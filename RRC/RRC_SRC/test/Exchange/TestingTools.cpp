/// \file   TestingTools.cpp
/// \author Artem A. Zhidkov
/// \date   20.01.2015
/// \brief  Утилиты для тестирования модуля Exchange

#include <TestingTools.h>
#include <fstream>

BoostFS::path testing_tools::TestingPathAbsolute()
{
  BoostFS::path aResult = aCurPath;
  aResult.append("TestLogs/");
  return aResult;
}

BoostFS::path testing_tools::TestingPathRelative()
{
  BoostFS::path aResult("./TestLogs/");
  return aResult;
}


testing_tools::ConfigFileTest::ConfigFileTest()
{
  myGridSize[0] = 3;
  myGridSize[1] = 2;
  myGridSize[2] = 2;
  myPartSizes = "NORMAL 1.0 0.1";
  myPartDist = "NORMAL 10.0 0.5";

  myChargeSource = "VALUE";
  myChargeType = "CHARGE_ARRAY";
  myChargeValue = "1.0 2.0 3.0 4.0 5.0 6.0 7.0 8.0 9.0 10.0 11.0 12.0";

  myRiseFall = 0.6;
  myExtField[0] = 0.1;
  myExtField[1] = 0.2;
  myExtField[2] = 1.0;
  myOutFile = "./result/res.txt";
}

void testing_tools::ConfigFileTest::CreateConfig(const BoostFS::path& theConfigFile) const
{
  std::fstream aFile(theConfigFile.string().c_str(), std::fstream::out);
  if (!aFile.is_open())
  {
    if (!BoostFS::create_directory(theConfigFile))
      return;
    aFile.open(theConfigFile.string().c_str(), std::fstream::out);
  }

  aFile << "[PARTICLE]" << std::endl
        << "X_SIZE = "           << myGridSize[0] << std::endl
        << "Y_SIZE = "           << myGridSize[1] << std::endl
        << "Z_SIZE = "           << myGridSize[2] << std::endl
        << "SIZES = "            << myPartSizes << std::endl
        << "DISTANCES = "        << myPartDist << std::endl << std::endl
        << "CHARGE_SOURCE = "    << myChargeSource << std::endl
        << myChargeType << " = " << myChargeValue << std::endl << std::endl
        << "[CONNECTION]" << std::endl
        << "RISE_FALL = "        << myRiseFall << std::endl << std::endl
        << "[EXTERNAL_FIELD]" << std::endl
        << "X = "                << myExtField[0] << std::endl
        << "Y = "                << myExtField[1] << std::endl
        << "Z = "                << myExtField[2] << std::endl << std::endl
        << "[OUTPUT]" << std::endl
        << "FILE = "             << myOutFile << std::endl;

  aFile.close();
}
