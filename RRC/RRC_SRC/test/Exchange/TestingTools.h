/// \file   TestingTools.h
/// \author Artem A. Zhidkov
/// \date   20.01.2015
/// \brief  Утилиты для тестирования модуля Exchange

#ifndef _TESTINGTOOLS_H_
#define _TESTINGTOOLS_H_

#include <Exchange.h>
#include <string>

/** \brief Пространство имён, содержащее набор вспомогательных функций
 *         для настройки параметров тестирования
 */
namespace testing_tools
{
  static const BoostFS::path aCurPath = BoostFS::current_path();

  /** \brief Абсолютный путь до временной директории для тестирования
   */
  BoostFS::path TestingPathAbsolute();

  /** \brief Относительный путь до временной директории для тестирования
   */
  BoostFS::path TestingPathRelative();

  /** \brief Структура, хранящая тестовую информацию для конфигурационного файла.
   *         Осуществляет генерацию конфигурационного файла.
   */
  struct ConfigFileTest
  {
    /// \brief Заполнение полей структуры значениями по умолчанию
    ConfigFileTest();

    /// \brief Генерация конфигурационного файла
    void CreateConfig(const BoostFS::path& theConfigFile) const;

    // ====================================================

    int         myGridSize[3];  ///< Размеры сетки по X, Y и Z
    std::string myPartSizes;    ///< Распределение размеров частиц
    std::string myPartDist;     ///< Распределение расстояний между частицами

    std::string myChargeSource; ///< Источник получения зарядов: текущий конфигурационный файл или внешний файл
    std::string myChargeType;   ///< Способ определения источников: массив значений, случайная величина, функция
    std::string myChargeValue;  ///< Текстовое представление информации о зарядах

    double      myRiseFall;     ///< Вероятность возникновения/разрушения связей
    double      myExtField[3];  ///< Компоненты внешнего электрического поля
    std::string myOutFile;      ///< Имя файла с результатами расчёта
  };
}

#endif // _TESTINGTOOLS_H_
