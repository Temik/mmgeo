/// \file   ExchangeTest.cpp
/// \author Artem A. Zhidkov
/// \date   18.01.2015
/// \brief  Набор тестов для модуля Exchange

// Опция BOOST_TEST_DYN_LINK используется для динамической линковки с внешними библиотеками.
// В настоящее время используется статическая линковка в Visual C++ и динамическая в g++
#ifdef __GNUC__
#define BOOST_TEST_DYN_LINK
#endif
#define BOOST_TEST_MODULE Exchange

#include <boost/test/unit_test.hpp>

#include <RRC_Configure.h>
#include <Exchange_ConfigReader.h>
#include <TestingTools.h>

#include <string>
#include <sstream>


BOOST_AUTO_TEST_SUITE(ConfigReader)

  BOOST_AUTO_TEST_SUITE(Construction)

    // ========================================================================
    /** \test Проверка корректности создания экземпляра класса
     *        Exchange_ConfigReader
     */
    BOOST_AUTO_TEST_CASE(Test1)
    {
      Exchange::ConfigReader* aConfReader = Exchange::ConfigReader::GetInstance();
      BOOST_REQUIRE_NE(aConfReader, (Exchange::ConfigReader*)0);
    }

  BOOST_AUTO_TEST_SUITE_END()



  BOOST_AUTO_TEST_SUITE(ConfigTemplate)

    // ========================================================================
    /** \test Проверка корректности копирования шаблона конфигурационного файла
     *        в директорию по абсолютному пути
     */
    BOOST_AUTO_TEST_CASE(Test1)
    {
      BoostFS::path aTest = testing_tools::TestingPathAbsolute();

      /// 1. Проверка создания объекта Exchange_ConfigReader
      Exchange::ConfigReader* aConfReader = Exchange::ConfigReader::GetInstance();
      BOOST_CHECK(aConfReader);

      aConfReader->CopyTemplateTo(aTest.string());
      /// 2. Проверка корректности статуса после копирования шаблона
      BOOST_CHECK_EQUAL(aConfReader->Status(), Exchange::STATUS_OK);

      /// 3. Проверка, что файл создан
      BoostFS::path aTemplate(RRC_DEFAULT_CONFIG_TEMPLATE);
      aTest.append(aTemplate.filename().string());
      BOOST_CHECK(BoostFS::exists(aTest));
    }

    // ========================================================================
    /** \test Проверка корректности копирования шаблона конфигурационного файла
     *        в файл по абсолютному пути
     */
    BOOST_AUTO_TEST_CASE(Test2)
    {
      BoostFS::path aTest = testing_tools::TestingPathAbsolute();
      aTest.append("ConfigTemplate2.conf");
      if (BoostFS::exists(aTest))
      {
        /// 0. Если файл уже существует, пробуем удалить его.
        BOOST_CHECK(BoostFS::remove(aTest));
      }

      /// 1. Проверка создания объекта Exchange_ConfigReader
      Exchange::ConfigReader* aConfReader = Exchange::ConfigReader::GetInstance();
      BOOST_CHECK(aConfReader);

      aConfReader->CopyTemplateTo(aTest.string());
      /// 2. Проверка корректности статуса после копирования шаблона
      BOOST_CHECK_EQUAL(aConfReader->Status(), Exchange::STATUS_OK);

      /// 3. Проверка, что файл создан
      BOOST_CHECK(BoostFS::exists(aTest));
    }

    // ========================================================================
    /** \test Проверка корректности копирования шаблона конфигурационного файла
     *        в директорию по относительному пути
     */
    BOOST_AUTO_TEST_CASE(Test3)
    {
      BoostFS::path aTest = testing_tools::TestingPathRelative();

      /// 1. Проверка создания объекта Exchange_ConfigReader
      Exchange::ConfigReader* aConfReader = Exchange::ConfigReader::GetInstance();
      BOOST_CHECK(aConfReader);

      aConfReader->CopyTemplateTo(aTest.string());
      /// 2. Проверка корректности статуса после копирования шаблона
      BOOST_CHECK_EQUAL(aConfReader->Status(), Exchange::STATUS_OK);

      /// 3. Проверка, что файл создан
      BoostFS::path aTemplate(RRC_DEFAULT_CONFIG_TEMPLATE);
      aTest.append(aTemplate.filename().string());
      BOOST_CHECK(BoostFS::exists(aTest));
    }

    // ========================================================================
    /** \test Проверка корректности копирования шаблона конфигурационного файла
     *        в файл по относительному пути
     */
    BOOST_AUTO_TEST_CASE(Test4)
    {
      BoostFS::path aTest = testing_tools::TestingPathRelative();
      aTest.append("ConfigTemplate4.conf");
      if (BoostFS::exists(aTest))
      {
        /// 0. Если файл уже существует, пробуем удалить его.
        BOOST_CHECK(BoostFS::remove(aTest));
      }

      /// 1. Проверка создания объекта Exchange_ConfigReader
      Exchange::ConfigReader* aConfReader = Exchange::ConfigReader::GetInstance();
      BOOST_CHECK(aConfReader);

      aConfReader->CopyTemplateTo(aTest.string());
      /// 2. Проверка корректности статуса после копирования шаблона
      BOOST_CHECK_EQUAL(aConfReader->Status(), Exchange::STATUS_OK);

      /// 3. Проверка, что файл создан
      BOOST_CHECK(BoostFS::exists(aTest));
    }

    // ========================================================================
    /** \test Проверка корректности обработки ошибки при копировании
     *        в Read-Only директорию
     */
    BOOST_AUTO_TEST_CASE(Test5)
    {
      /// 0. Создание папки с правами только на чтение
      BoostFS::path aTest = testing_tools::TestingPathAbsolute();
      aTest.append("Test5/");
      if (!BoostFS::exists(aTest))
        BoostFS::create_directory(aTest);
      BoostFS::permissions(aTest, BoostFS::others_read | BoostFS::owner_read | BoostFS::group_read);

      /// 1. Проверка создания объекта Exchange_ConfigReader
      Exchange::ConfigReader* aConfReader = Exchange::ConfigReader::GetInstance();
      BOOST_CHECK(aConfReader);

      aConfReader->CopyTemplateTo(aTest.string() + "/");
      /// 2. Проверка корректности статуса после копирования шаблона
      BOOST_CHECK_EQUAL(aConfReader->Status(), Exchange::STATUS_UNABLECOPY);
    }

  BOOST_AUTO_TEST_SUITE_END()



  BOOST_AUTO_TEST_SUITE(ConfigReader)

    /// \brief Сравнение исходных данных с результатами, полученными из конфигурационного файла
    void compareResults(const testing_tools::ConfigFileTest& theConfigBase,
                              Exchange::ConfigReader*        theConfigReader)
    {
      // 1. Сравнение размеров сетки
      unsigned long aSizeX, aSizeY, aSizeZ;
      theConfigReader->GridSizes(aSizeX, aSizeY, aSizeZ);
      BOOST_CHECK_EQUAL(theConfigBase.myGridSize[0], aSizeX);
      BOOST_CHECK_EQUAL(theConfigBase.myGridSize[1], aSizeY);
      BOOST_CHECK_EQUAL(theConfigBase.myGridSize[2], aSizeZ);

      // 2. Проверка зарядов частиц
      std::ostringstream aStream;
      for (unsigned long x = 0; x < aSizeX; x++)
        for (unsigned long y = 0; y < aSizeY; y++)
          for (unsigned long z = 0; z < aSizeZ; z++)
            aStream << theConfigReader->ParticleCharge(x, y, z) << " ";
      std::string aStr = aStream.str();
      std::string::iterator aStart, aFinish;
      aStart = aStr.begin();
      while (*aStart == ' ')
        aStart++;
      aFinish = aStr.end() - 1;
      while (*aFinish == ' ')
        aFinish--;
      aStr = aStr.substr(aStart - aStr.begin(), aFinish - aStart);
      BOOST_CHECK_EQUAL(theConfigBase.myChargeValue, aStr);

      // 3. Проверка вероятности возникновения/разрушения связей
      BOOST_CHECK_EQUAL(theConfigBase.myRiseFall, theConfigReader->ConnectionRiseFall());

      // 4. Проверка координат внешнего электрического поля
      double anExtField[3];
      theConfigReader->ExternalField(anExtField[0], anExtField[1], anExtField[2]);
      BOOST_CHECK_EQUAL(theConfigBase.myExtField[0], anExtField[0]);
      BOOST_CHECK_EQUAL(theConfigBase.myExtField[1], anExtField[1]);
      BOOST_CHECK_EQUAL(theConfigBase.myExtField[2], anExtField[2]);

      // 5. Проверка на совпадение имени файла для записи результата
      BOOST_CHECK_EQUAL(theConfigBase.myOutFile, theConfigReader->GetOutputFileName());
    }

    // ========================================================================
    /** \test Проверка корректности чтения конфигурационного файла
     *        (значения по умолчанию):
     *        размеры частиц и расстояния имеют нормальное распределение;
     *        заряды заданы массивом в текущем файле
     */
    BOOST_AUTO_TEST_CASE(Test1)
    {
      /// 0. Задаём имя конфигурационного файла
      BoostFS::path aFile = testing_tools::TestingPathAbsolute();
      aFile.append("task.conf");

      /// 0. Пишем конфигурационный файла на диск
      testing_tools::ConfigFileTest aConfig;
      aConfig.CreateConfig(aFile);

      /// 1. Читаем записанный конфигурационный файл
      Exchange::ConfigReader* aConfReader = Exchange::ConfigReader::GetInstance();
      BOOST_CHECK(aConfReader);
      aConfReader->ProcessConfigFile(aFile.string());
      BOOST_REQUIRE_EQUAL(aConfReader->Status(), Exchange::STATUS_OK);

      /// 2. Проверяем корректность параметров
      compareResults(aConfig, aConfReader);
    }

  BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE_END()
