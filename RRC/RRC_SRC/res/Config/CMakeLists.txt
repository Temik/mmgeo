# File:   RRC_SRC/res/Config/CMakeLists.txt
# Author: Artem A. Zhidkov
# Date:   Dec 22, 2014

SET(RESOURCE_FILES
    template.conf
)

INSTALL(FILES ${RESOURCE_FILES} DESTINATION "${RRC_RES}/Config")
