\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{reqtmpl}[2011/10/13 A class for making requirements and documentation.]

\LoadClass[a4paper,12pt]{article}
\RequirePackage{longtable}
\RequirePackage{ifthen}
\RequirePackage[utf8]{inputenc}
\RequirePackage[russian]{babel}
\RequirePackage{indentfirst}
\RequirePackage{xcolor}
\RequirePackage{varioref}
\RequirePackage{lastpage}

\definecolor{darkblue}{rgb}{0,0,0.5}
\RequirePackage[colorlinks,unicode,urlcolor=darkblue]{hyperref}

\RequirePackage{fancyhdr}

\setlength{\textheight}{257mm}%
\setlength{\textwidth}{180mm}%
\setlength{\hoffset}{-1in}%
\setlength{\voffset}{-1in}%
\setlength{\oddsidemargin}{20mm}
\setlength{\evensidemargin}{20mm}
\setlength{\topmargin}{5mm}
\setlength{\headheight}{15mm}
\setlength{\headsep}{5mm}
\setlength{\footskip}{5mm}


\newcommand{\tbd}{{\color{red}Будет определено}}

\gdef\@company{}
\def\company#1{\gdef\@company{#1}}

\gdef\@targets{}
\def\targets#1{\gdef\@targets{#1}}
\gdef\@usecase{}
\def\usecases#1{\gdef\@usecase{#1}}
\gdef\@verview{}
\def\overview#1{\gdef\@verview{#1}}

% Вспомогательные команды
\gdef\@writemode{0}
\def\aznewline{%
  \ifnum\@writemode=0
    \noexpand\aznewline%
  \else
    \newline%
  \fi%
}
\def\azsmall{%
  \ifnum\@writemode=0
    \noexpand\azsmall%
  \else
    \small%
  \fi%
}
\def\aztextit#1{%
  \ifnum\@writemode=0
    \noexpand\aztextit{#1}%
  \else
    \textit{#1}%
  \fi%
}

% Оформление списков
\def\azitem[#1]{%
  \ifnum\@writemode=0
    \noexpand\azitem[#1]
  \else
    \item[#1]%
  \fi%
}
\def\azbibitem#1{%
  \ifnum\@writemode=0
    \noexpand\azbibitem{#1}
  \else
    \bibitem{#1}%
  \fi%
}

% Термины и определения
\gdef\terms@bbrv{}
\def\showterms{%
  \gdef\@writemode{1}%
  \begin{description}\terms@bbrv\end{description}%
  \gdef\@writemode{0}%
}
\def\addterm@one#1#2{%
  \edef\terms@bbrv{\terms@bbrv \azitem[#1] --- #2}
}
\def\addterm@two#1[#2]#3{%
  \edef\terms@bbrv{\terms@bbrv \azitem[#1] (#2) --- #3}
}
\def\addterm#1{%
  \@ifnextchar[{\addterm@two{#1}}{\addterm@one{#1}}%
}

% Ссылки на внешние источники
\gdef\@links{}
\def\showlinks{
  \begingroup
  \renewcommand{\section}[2]{}% remove bibliography title
  \gdef\@writemode{1}%
  \begin{thebibliography}{99}\@links\end{thebibliography}%
  \gdef\@writemode{0}%
  \endgroup
}
\def\azhref#1#2{%
  \ifnum\@writemode=0
    \noexpand\azhref{#1}{#2}%
  \else
    \href{#1}{#2}%
  \fi%
}
\def\azurl#1{%
  \ifnum\@writemode=0
    \noexpand\azurl{#1}%
  \else%
    \url{#1}%
  \fi}

\def\addlink@withdescription#1#2[#3]{%
  \edef\@links{\@links \azbibitem{#1} #2 \aznewline{\azsmall #3}}
}
\def\addlink@withoutdescription#1#2{%
  \edef\@links{\@links \azbibitem{#1} #2}
}
\def\addlink#1#2{%
  \@ifnextchar[{\addlink@withdescription{#1}{#2}}{\addlink@withoutdescription{#1}{#2}}%
}

% Ссылки на внешние документы
\newcounter{extlinks}
\setcounter{extlinks}{0}
\def\adddoclink@withurl#1#2#3{%
  \addlink{#1}{#2\space(\azurl{#3})}%
}
\def\adddoclink@withref[#1]#2{%
  \@ifnextchar\bgroup{\adddoclink@withurl{#1}{#2}}{\addlink{#1}{#2}}%
}
\def\adddoclink@withoutref{%
  \stepcounter{extlinks}%
  \adddoclink@withref[ExtRef\arabic{extlinks}]%
}
\def\adddoclink{%
  \@ifnextchar[{\adddoclink@withref}{\adddoclink@withoutref}%
}

% Ссылки на сайты
\def\addurllink@withref[#1]#2{%
  \addlink{#1}{\azurl{#2}}%
}
\def\addurllink@withoutref{%
  \stepcounter{extlinks}%
  \addurllink@withref[ExtRef\arabic{extlinks}]%
}
\def\addurllink{%
  \@ifnextchar[{\addurllink@withref}{\addurllink@withoutref}%
}

% Библиографические ссылки
\def\addbiblink@withref[#1]#2#3#4{%
  \adddoclink@withref[#1]{{#2}\space\aztextit{#3}\space//\space{#4}}%
}
\def\addbiblink@withoutref{%
  \stepcounter{extlinks}%
  \addbiblink@withref[ExtRef\arabic{extlinks}]%
}
\def\addbiblink{%
  \@ifnextchar[{\addbiblink@withref}{\addbiblink@withoutref}%
}


% Основные возможности
\@namedef{fe@tures}{}
\providecommand\features[1]{\@namedef{fe@tures}{#1}}

% Описание интерфейса
\@namedef{interf@ce}{}
\providecommand\interface[1]{\@namedef{interf@ce}{#1}}

% Ограничения продукта
\@namedef{constr@ints}{}
\providecommand\constraints[1]{\@namedef{constr@ints}{#1}}

% Детальные требования
\gdef\det@iled{}
\newcounter{detcount}

\newcommand{\detlink}[2]{%
  SRS\_{#1}\_%
  \ifthenelse{#2<10}{00{#2}}{%
    \ifthenelse{#2<100}{0{#2}}{#2}%
  }%
}
\newcommand{\detcounter}[2]{%
  \textbf{\detlink{#1}{\arabic{#2}}.}%
}

\newenvironment{detailed}[2]{%
  \subsection{#2}
  \setcounter{detcount}{0}
  \labelformat{detcount}{\string\detlink{#1}{\arabic{detcount}}}
  \begin{list}{\detcounter{#1}{detcount}}{\usecounter{detcount}}%
}{%
  \end{list}%
}

\newcommand{\ditem}[1]{% Пункт детальных требований
  \item {\bf #1} \par
}

% Титульный лист
\renewcommand{\maketitle}{%
  \pagebreak
  \thispagestyle{empty}
  \begin{center}
  \large
  \@company
  \vfill\vfill
  {\Large \bf \@title\par}
  \vskip 1em
  \@author\par
  \vskip 2em
  Листов \pageref{LastPage}
  \vfill\vfill\vfill
  \number\year
  \end{center}
  \pagebreak
}


% Вывод краткого описания документа
\newcommand{\writebrief}{%
  \lhead{}
  \chead{\thepage \\ \textsc{\@title}}
  \rhead{}
  \lfoot{}
  \cfoot{}
  \rfoot{}
  \pagestyle{fancy}
  \maketitle\clearpage%
  \tableofcontents\clearpage%
  \section{Введение}
    \subsection{Цели}
      \ifthenelse{\equal{\@targets}{}}{\tbd}{\@targets}
    \subsection{Область и границы применения}
      \ifthenelse{\equal{\@usecase}{}}{\tbd}{\@usecase}
    \subsection{Термины, аббревиатуры, сокращения}
      \ifthenelse{\equal{\terms@bbrv}{}}{\tbd}{\showterms}
    \subsection{Ссылки}
      \ifthenelse{\equal{\@links}{}}{\tbd}{\showlinks}
    \subsection{Обзор}
      \ifthenelse{\equal{\@verview}{}}{\tbd}{\@verview}
  \newpage
  \section{Общее описание}
    \subsection{Основные возможности}
      \ifthenelse{\equal{\fe@tures}{}}{\tbd}{\fe@tures}
    \subsection{Интерфейс}
      \ifthenelse{\equal{\interf@ce}{}}{\tbd}{\interf@ce}
    \subsection{Ограничения программного продукта}
      \ifthenelse{\equal{\constr@ints}{}}{\tbd}{\constr@ints}
  \newpage
  \section{Детальные требования}
}
