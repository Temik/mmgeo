# File:   RRC_SRC/CMakeLists.txt
# Author: Artem A. Zhidkov
# Date:   Dec 8, 2014

# Project preferences
SET(PROJECT_NAME "RRC")

# Minimal requirements
CMAKE_MINIMUM_REQUIRED(VERSION 3.0 FATAL_ERROR)

# Project name and used languages
PROJECT(${PROJECT_NAME} C CXX)
# Project version
SET(RRC_VERSION_MAJOR 0)
SET(RRC_VERSION_MINOR 1)
SET(RRC_VERSION "${RRC_VERSION_MAJOR}.${RRC_VERSION_MINOR}")
SET(PROJECT_VERSION ${RRC_VERSION})

# Basic settings
SET(CMAKE_INCLUDE_CURRENT_DIR TRUE)

# Change the target path variables to meet Unix style
IF(UNIX)
  SET(RRC_SRC_DIR $ENV{RRC_SRC_DIR})
  SET(RRC_DOC_DIR $ENV{RRC_DOC_DIR})
  SET(RRC_BUILD_DIR $ENV{RRC_BUILD_DIR})
  SET(RRC_INSTALL_DIR $ENV{RRC_INSTALL_DIR})
  SET(EIGEN_INCLUDE_DIR $ENV{EIGEN_INCLUDE_DIR})
ELSE(UNIX)
  STRING(REPLACE "\\" "/" RRC_SRC_DIR $ENV{RRC_SRC_DIR})
  STRING(REPLACE "\\" "/" RRC_DOC_DIR $ENV{RRC_DOC_DIR})
  STRING(REPLACE "\\" "/" RRC_BUILD_DIR $ENV{RRC_BUILD_DIR})
  STRING(REPLACE "\\" "/" RRC_INSTALL_DIR $ENV{RRC_INSTALL_DIR})
  STRING(REPLACE "\\" "/" EIGEN_INCLUDE_DIR $ENV{EIGEN_INCLUDE_DIR})
ENDIF(UNIX)

# Additional path variables
SET(RRC_BIN "${RRC_INSTALL_DIR}/bin")
SET(RRC_LIB "${RRC_INSTALL_DIR}/lib")
SET(RRC_INC "${RRC_INSTALL_DIR}/include")
SET(RRC_RES "${RRC_INSTALL_DIR}/resource")

SET(RRC_DEFAULT_CONFIG_TEMPLATE
    "${RRC_RES}/Config/template.conf"
)

# Configure file to pass CMake setting into the source code
CONFIGURE_FILE("${RRC_SRC_DIR}/RRC_Configure.h.in"
               "${RRC_BUILD_DIR}/RRC_Configure.h")

# Add build dir to include paths, so that RRC_Configure.h will be found
INCLUDE_DIRECTORIES("${RRC_BUILD_DIR}")

# Paths to search additional modules
SET(CMAKE_MODULE_PATH "${CMAKE_SOURCE_DIR}/CMakeCommon" ${CMAKE_MODULE_PATH})

# Searching additional modules
SET(Boost_NO_SYSTEM_PATHS ON)
IF(NOT UNIX)
  SET(Boost_USE_STATIC_LIBS ON)
ENDIF()
FIND_PACKAGE(Boost 1.57 REQUIRED COMPONENTS program_options system filesystem random unit_test_framework)

# Documentation generator
IF($ENV{RRC_BUILD_DOC})
  FIND_PACKAGE(Doxygen)
  IF(DOXYGEN_FOUND)
    SET(DOXYFILE_IN ${RRC_SRC_DIR}/dox/Doxyfile.in)
    SET(DOXYFILE    ${RRC_BUILD_DIR}/Doxyfile)
    SET(DOXY_INPUT  ${RRC_SRC_DIR})
    SET(DOXY_OUTPUT ${RRC_DOC_DIR})

    CONFIGURE_FILE(${DOXYFILE_IN} ${DOXYFILE} @ONLY)

    ADD_CUSTOM_TARGET(doc
                      COMMAND ${DOXYGEN_EXECUTABLE} ${DOXYFILE}
                      SOURCES ${DOXYFILE})
  ENDIF(DOXYGEN_FOUND)
ENDIF($ENV{RRC_BUILD_DOC})

# Unit testing
ENABLE_TESTING()

# Subdirectories to be parsed
ADD_SUBDIRECTORY(src)
ADD_SUBDIRECTORY(res)
ADD_SUBDIRECTORY(test)
