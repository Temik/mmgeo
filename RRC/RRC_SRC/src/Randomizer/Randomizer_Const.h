/// \file   Randomizer_Const.h
/// \author Artem A. Zhidkov
/// \date   09.01.2015
/// \brief  Описание класса для генераторов постоянных чисел

#ifndef _RANDOMIZER_CONST_H_
#define _RANDOMIZER_CONST_H_

#include <Randomizer_Distribution.h>

namespace Randomizer
{
  /** \class Randomizer::Const
   *  \ingroup Random
   *  \brief Генератор постоянных чисел. Всегда выдаётся одно и то же число
   */
  class Const : public Distribution
  {
  protected:
    Const();

  public:
    RANDOMIZER_EXPORT virtual ~Const() {}

    /// \brief Возвращает значение случайной величины
    RANDOMIZER_EXPORT virtual double Value();

  protected:
    friend class Randomizer::Factory;
  };

}

#endif // _RANDOMIZER_CONST_H_
