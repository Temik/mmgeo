/// \file   Randomizer_Normal.cpp
/// \author Artem A. Zhidkov
/// \date   09.01.2015
/// \brief  Реализация класса для генераторов случайных чисел с нормальным распределением

#include <Randomizer_Normal.h>

#define _RND_NORMAL_NBPARAMS 2

Randomizer::Normal::Normal()
{
  myType = RND_NORMAL;
  myNbParams = _RND_NORMAL_NBPARAMS;
}

double Randomizer::Normal::Value()
{
  // Если boost-генератор ещё не проинициализирован, инициализируем его
  if (!myIsInit)
  {
    if (Status() != STATUS_RND_OK)
    {
      std::string anException("Randomizer::Normal::Value(): Incorrect parameters");
      throw anException;
    }

    const double& aMean = myParameters[0];
    const double& aSigma = myParameters[1];
    BoostRnd::normal_distribution<double>::param_type aParameters(aMean, aSigma);
    myDistribution.param(aParameters);
  }
  return myDistribution(myBaseGen);
}

