/// \file   Randomizer_Boolean.h
/// \author Artem A. Zhidkov
/// \date   31.01.2015
/// \brief  Описание класса для генераторов булевых случайных чисел

#ifndef _RANDOMIZER_BOOLEAN_H_
#define _RANDOMIZER_BOOLEAN_H_

#include <Randomizer_Uniform.h>

namespace Randomizer
{
  /** \class Randomizer::Boolean
   *  \ingroup Random
   *  \brief Генератор булевых случайных чисел. Выдаёт одно из двух значений (0 или 1).
   *
   *  Генератор основывается на генераторе равномерной случайной величины, распределённой
   *  на отрезке [0,1]. Выдаваемый результат зависит от заданного порога: если случайная
   *  величина превышает данный порог, будет сгенерировано значение 1, в противном случае - 0.
   */
  class Boolean : public Uniform
  {
  protected:
    Boolean();

  public:
    RANDOMIZER_EXPORT virtual ~Boolean() {}

    /// \brief Возвращает значение случайной величины
    RANDOMIZER_EXPORT virtual double Value();

  protected:
    friend class Randomizer::Factory;
  };

}

#endif // _RANDOMIZER_BOOLEAN_H_
