/// \file   Randomizer_Normal.h
/// \author Artem A. Zhidkov
/// \date   09.01.2015
/// \brief  Описание класса для генераторов случайных чисел с нормальным распределением

#ifndef _RANDOMIZER_NORNAL_H_
#define _RANDOMIZER_NORNAL_H_

#include <Randomizer_Distribution.h>

namespace Randomizer
{
  /** \class Randomizer::Normal
   *  \ingroup Random
   *  \brief Генератор случайных чисел распределённых по закону Гаусса с параметрами
   *         \f$\mu\f$ - математическое ожидание, \f$\sigma\f$ - среднеквадратичное отклонение
   */
  class Normal : public Distribution
  {
  protected:
    Normal();

  public:
    RANDOMIZER_EXPORT virtual ~Normal() {}

    /// \brief Возвращает значение случайной величины
    RANDOMIZER_EXPORT virtual double Value();

  protected:
    BoostRnd::normal_distribution<double> myDistribution; ///< Нормальное распределение случайной величины

    friend class Randomizer::Factory;
  };

}

#endif // _RANDOMIZER_NORNAL_H_
