/// \file   Randomizer_Uniform.h
/// \author Artem A. Zhidkov
/// \date   09.01.2015
/// \brief  Описание класса для генераторов случайных чисел с равномерным распределением

#ifndef _RANDOMIZER_UNIFORM_H_
#define _RANDOMIZER_UNIFORM_H_

#include <Randomizer_Distribution.h>

namespace Randomizer
{
  /** \class Randomizer::Uniform
   *  \ingroup Random
   *  \brief Генератор случайных чисел равномерно распределённых на отрезке [a,b]
   */
  class Uniform : public Distribution
  {
  protected:
    Uniform();

  public:
    RANDOMIZER_EXPORT virtual ~Uniform() {}

    /// \brief Возвращает значение случайной величины
    RANDOMIZER_EXPORT virtual double Value();

  protected:
    BoostRnd::uniform_real_distribution<double> myDistribution; ///< Равномерное распределение случайной величины

    friend class Randomizer::Factory;
  };

}

#endif // _RANDOMIZER_UNIFORM_H_
