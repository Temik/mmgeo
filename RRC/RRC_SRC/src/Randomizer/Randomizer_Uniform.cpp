/// \file   Randomizer_Uniform.cpp
/// \author Artem A. Zhidkov
/// \date   09.01.2015
/// \brief  Реализация класса для генераторов случайных чисел с равномерным распределением

#include <Randomizer_Uniform.h>

#define _RND_UNIFORM_NBPARAMS 2

Randomizer::Uniform::Uniform()
{
  myType = RND_UNIFORM;
  myNbParams = _RND_UNIFORM_NBPARAMS;
}

double Randomizer::Uniform::Value()
{
  // Если boost-генератор ещё не проинициализирован, инициализируем его
  if (!myIsInit)
  {
    if (Status() != STATUS_RND_OK)
    {
      std::string anException("Randomizer::Uniform::Value(): Incorrect parameters");
      throw anException;
    }

    double aMin;
    double aMax;
    if (myParameters[0] < myParameters[1])
    {
      aMin = myParameters[0];
      aMax = myParameters[1];
    }
    else
    {
      aMin = myParameters[1];
      aMax = myParameters[0];
    }
    BoostRnd::uniform_real_distribution<double>::param_type aParameters(aMin, aMax);
    myDistribution.param(aParameters);
  }
  return myDistribution(myBaseGen);
}

