/// \file   Randomizer_Factory.h
/// \author Artem A, Zhidkov
/// \date   06.01.2015
/// \brief  Описание фабрики генераторов случайных чисел

#ifndef _RANDOMIZER_FACTORY_H_
#define _RANDOMIZER_FACTORY_H_

#include <Randomizer.h>
#include <Randomizer_Distribution.h>

namespace Randomizer
{
  /** \class Randomizer::Factory
   *  \ingroup Random
   *  \brief Фабрика генераторов случайных чисел
   *
   *  Позволяет создавать новые генераторы на основе их типа и набора параметров.
   */
  class Factory
  {
  public:
    /** \brief Создание генератора на основе его типа и параметров
     *  \param[in] theType         тип генератора
     *  \param[in] theFirstParam   первый параметр
     *  \param[in] theSecondParam  второй параметр
     *  \return Указатель на созданный объект
     */
    RANDOMIZER_EXPORT static RandomDistrPtr CreateGenerator(
                                  const Randomizer::Type& theType,
                                  const double&           theFirstParam,
                                  const double&           theSecondParam = 0.0);

    /** \brief Создание генератора на основе строкового представления
     *  \param[in] theParameters  тип и параметры генератора в виде строки
     *  \return Указатель на созданный объект
     */
    RANDOMIZER_EXPORT static RandomDistrPtr CreateGenerator(
                                  const std::string& theParameters);
  };

}

#endif // _RANDOMIZER_FACTORY_H_
