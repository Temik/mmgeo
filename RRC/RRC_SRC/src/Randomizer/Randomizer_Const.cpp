/// \file   Randomizer_Const.cpp
/// \author Artem A. Zhidkov
/// \date   09.01.2015
/// \brief  Реализация класса для генераторов постоянных чисел

#include <Randomizer_Const.h>

#define _RND_CONST_NBPARAMS 1

Randomizer::Const::Const()
{
  myType = RND_CONST;
  myNbParams = _RND_CONST_NBPARAMS;
  myStatus = STATUS_RND_LESS;
}

double Randomizer::Const::Value()
{
  if (Status() != STATUS_RND_OK)
  {
    std::string anException("Randomizer::Const::Value(): Incorrect parameters");
    throw anException;
  }
  return myParameters.front();
}

