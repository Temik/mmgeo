/// \file   Randomizer_Factory.cpp
/// \author Artem A, Zhidkov
/// \date   06.01.2015
/// \brief  Реализация фабрики генераторов случайных чисел

#include <Randomizer_Factory.h>
#include <Randomizer_Boolean.h>
#include <Randomizer_Const.h>
#include <Randomizer_Normal.h>
#include <Randomizer_Uniform.h>

#include <sstream>
#include <boost/algorithm/string.hpp>


/// \brief Преобразование строки к типу генератора
static Randomizer::Type StringToType(const std::string& theTypeStr);


RandomDistrPtr Randomizer::Factory::CreateGenerator(
    const Randomizer::Type& theType,
    const double&           theFirstParam,
    const double&           theSecondParam)
{
  RandomDistrPtr aResult;
  switch (theType)
  {
  case RND_UNIFORM:
    aResult = RandomDistrPtr(new Randomizer::Uniform);
    break;
  case RND_NORMAL:
    aResult = RandomDistrPtr(new Randomizer::Normal);
    break;
  case RND_CONST:
    aResult = RandomDistrPtr(new Randomizer::Const);
    break;
  case RND_BOOLEAN:
    aResult = RandomDistrPtr(new Randomizer::Boolean);
    break;
  }
  if (!aResult)
    return aResult;

  aResult->AddParameter(theFirstParam);
  if (aResult->Status() == STATUS_RND_LESS)
    aResult->AddParameter(theSecondParam);

  if (aResult->Status() != STATUS_RND_OK)
    return RandomDistrPtr();
  return aResult;
}

RandomDistrPtr Randomizer::Factory::CreateGenerator(const std::string& theParameters)
{
  std::string aGenTypeStr;
  double aParam1 = 0.0;
  double aParam2 = 0.0;

  std::istringstream aStream(theParameters, std::istringstream::in);
  aStream >> aGenTypeStr;
  if (!aStream.eof())
    aStream >> aParam1;
  if (!aStream.eof())
    aStream >> aParam2;

  Randomizer::Type aGenType = StringToType(aGenTypeStr);
  if (aGenType == RND_UNKNOWN)
    return RandomDistrPtr();

  return CreateGenerator(aGenType, aParam1, aParam2);
}



// ===============  Auxiliary functions  ======================================
Randomizer::Type StringToType(const std::string& theTypeStr)
{
  std::string aTypeStr = boost::to_upper_copy(theTypeStr);

  if (aTypeStr == "UNIFORM")
    return Randomizer::RND_UNIFORM;
  else if (aTypeStr == "NORMAL")
    return Randomizer::RND_NORMAL;
  else if (aTypeStr == "CONST")
    return Randomizer::RND_CONST;
  else if (aTypeStr == "BOOL" || aTypeStr == "BOOLEAN")
    return Randomizer::RND_BOOLEAN;

  return Randomizer::RND_UNKNOWN;
}
