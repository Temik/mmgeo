/// \file   Randomizer_Distribution.cpp
/// \author Artem A. Zhidkov
/// \date   03.01.2015
/// \brief  Реализация базового класса для генераторов случайных чисел

#include <Randomizer_Distribution.h>

BoostRnd::taus88 Randomizer::Distribution::myBaseGen;

void Randomizer::Distribution::SetParameters(const std::vector<double>& theParams)
{
  if (theParams.size() > myNbParams)
    myStatus = STATUS_RND_GREATER;
  else if (theParams.size() < myNbParams)
    myStatus = STATUS_RND_LESS;
  else
  {
    myStatus = STATUS_RND_OK;
    myParameters = theParams;
  }
}

void Randomizer::Distribution::AddParameter(const double& theParam)
{
  if (myParameters.size() + 1 > myNbParams)
    myStatus = STATUS_RND_GREATER;
  else if (myParameters.size() + 1 < myNbParams)
    myStatus = STATUS_RND_LESS;
  else
  {
    myStatus = STATUS_RND_OK;
    myParameters.push_back(theParam);
  }
}
