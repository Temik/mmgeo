/// \file   Randomizer_Boolean.cpp
/// \author Artem A. Zhidkov
/// \date   31.01.2015
/// \brief  Реализация класса для генераторов булевых случайных чисел

#include <Randomizer_Boolean.h>

#define _RND_BOOLEAN_NBPARAMS 3

Randomizer::Boolean::Boolean()
{
  myType = RND_BOOLEAN;
  myNbParams = _RND_BOOLEAN_NBPARAMS;
  myStatus = STATUS_RND_LESS;

  // Устанавливаем параметры равномерного распределения
  AddParameter(0.0);
  AddParameter(1.0);
}

double Randomizer::Boolean::Value()
{
  if (Status() != STATUS_RND_OK)
  {
    std::string anException("Randomizer::Boolean::Value(): Incorrect parameters");
    throw anException;
  }
  double aVal = Randomizer::Uniform::Value();
  return (aVal <= myParameters.back() ? 0.0 : 1.0);
}

