/// \file   Randomizer.h
/// \author Artem A, Zhidkov
/// \date   03.01.2015
/// \brief  Основные определения модуля Randomizer

#ifndef _RANDOMIZER_H_
#define _RANDOMIZER_H_

#if defined WIN32
# if defined Randomizer_EXPORTS
#   define RANDOMIZER_EXPORT __declspec(dllexport)
# else
#   define RANDOMIZER_EXPORT __declspec(dllimport)
# endif
#else
# define RANDOMIZER_EXPORT
#endif

#include <boost/random.hpp>
namespace BoostRnd = boost::random;

namespace Randomizer
{
  /// \brief Корректность параметров генератора
  enum Status
  {
    STATUS_RND_OK,          ///< параметры заданы корректно
    STATUS_RND_LESS,        ///< параметров меньше, чем необходимо
    STATUS_RND_GREATER,     ///< количество параметров превышает необходимое
    STATUS_RND_UNKNOWNERROR ///< неизвестная ошибка
  };

  /// \brief Тип генератора случайных чисел
  enum Type
  {
    RND_UNIFORM, ///< равномерное распределение
    RND_NORMAL,  ///< нормальное распределение
    RND_CONST,   ///< постоянное значение
    RND_BOOLEAN, ///< булево распределение
    RND_UNKNOWN  ///< неизвестный тип случайной величины
  };
}

#endif // _RANDOMIZER_H_
