/// \file   Randomizer_Distribution.h
/// \author Artem A. Zhidkov
/// \date   03.01.2015
/// \brief  Описание базового класса для генераторов случайных чисел

#ifndef _RANDOMIZER_DISTRIBUTION_H_
#define _RANDOMIZER_DISTRIBUTION_H_

#include <Randomizer.h>

#include <boost/shared_ptr.hpp>
#include <vector>

namespace Randomizer
{
  class Factory;

  /** \class Randomizer::Distribution
   *  \ingroup Random
   *  \brief Базовый класс для генераторов случайных чисел
   *
   *  Каждый генератор определяется набором своих параметров.
   */
  class Distribution
  {
  protected:
    Distribution()
    {
      myIsInit = false;
      myStatus = STATUS_RND_LESS;
    }

  public:
    RANDOMIZER_EXPORT virtual ~Distribution() {}

    /** \brief Устанавливает параметры распределения случайной величины
     *  \param[in] theParams список параметров
     *
     *  Устанавливает соответствующий статус, если список параметров не соответствует типу случайной величины.
     */
    RANDOMIZER_EXPORT virtual void SetParameters(const std::vector<double>& theParams);

    /** \brief Добавляет параметр распределения случайной величины
     *  \param[in] theParam значение добавляемого параметра
     *
     *  Устанавливает соответствующий статус, если количество параметров превышает необходимое.
     */
    RANDOMIZER_EXPORT virtual void AddParameter(const double& theParam);

    /// \brief Возвращает значение случайной величины
    RANDOMIZER_EXPORT virtual double Value() = 0;

    /// \brief Возвращает корректность задания параметров генератора
    RANDOMIZER_EXPORT virtual const Randomizer::Status& Status() const
    { return myStatus; }

  protected:
    Randomizer::Type    myType;        ///< Тип генератора случайных чисел
    std::vector<double> myParameters;  ///< Параметры генератора
    size_t              myNbParams;    ///< Количество параметров для заданного генератора

    Randomizer::Status  myStatus;      ///< Статус. Определяет корректность задания параметров генератора

    static BoostRnd::taus88 myBaseGen; ///< Базовый генератор псевдо-случайных чисел
    bool                    myIsInit;  ///< Флаг показывает, что генератор boost проинициализирован корректно

    friend class Randomizer::Factory;
  };

}

typedef boost::shared_ptr<Randomizer::Distribution> RandomDistrPtr;

#endif // _RANDOMIZER_DISTRIBUTION_H_
