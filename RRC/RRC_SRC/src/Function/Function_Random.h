/// \file   Function_Random.h
/// \author Artem A, Zhidkov
/// \date   09.01.2015
/// \brief  Описание класса функций, задающих случайную величину

#ifndef _FUNCTION_RANDOM_H_
#define _FUNCTION_RANDOM_H_

#include <Function_Base.h>
#include <Randomizer_Distribution.h>

namespace Function
{
  /** \class Function::Random
   *  \ingroup Math
   *  \brief Класс функций, задающих случайную величину
   *
   *  Параметры при вызове метода Value() не важны
   *  (функция всегда генерирует случайное значение с заданным распределением).
   */
  class Random : public Base
  {
  protected:
    Random() : Base() {}

  public:
    FUNCTION_EXPORT virtual ~Random() {}

    /** \brief Осуществляет разбор входного потока, определяющего структуру функции
     *  \param[in] theData  входной поток
     *  \return Статус корректности данных во входном потоке
     *
     *  Формат функции следующий:
     *    [Тип случайной величины] [Список параметров]
     *
     *  Например, распределение Гаусса с мат. ожиданием 1.0 и среднеквадратичным отклонением 0.25:
     *    NORMAL 1.0 0.25
     */
    FUNCTION_EXPORT virtual const Function::Status& Parse(const std::istream& theData);
    /** \brief Осуществляет разбор входной строки, определяющей функцию
     *  \param[in] theData  входная строка
     *  \return Статус корректности заданной строки
     */
    FUNCTION_EXPORT virtual const Function::Status& Parse(const std::string& theData);

    /** \brief Вычисление значения функции в заданной точке
     *  \param[in] theParameters значения аргументов функции
     *  \return Значение функции при заданных параметрах
     */
    FUNCTION_EXPORT virtual double Value(const VarValueMap& theParameters) const;
    /** \brief Вычисление значения функции в заданной точке
     *  \param[in] theParameters значения аргументов функции
     *  \return Значение функции при заданных параметрах
     *
     *  Последовательность параметров должна соответствовать последовательности аргументов функции.
     */
    FUNCTION_EXPORT virtual double Value(const std::vector<double>& theParameters) const;
    /** \brief Вычисление значения функции в заданной точке
     *  \param[in] theX  первый аргумент функции
     *  \param[in] theY  второй аргумент функции
     *  \param[in] theZ  третий аргумент функции
     *  \return Значение функции при заданных параметрах
     *
     *  Последовательность параметров должна соответствовать последовательности аргументов функции.
     */
    FUNCTION_EXPORT virtual double Value(const double& theX,
                                         const double& theY = 0.0,
                                         const double& theZ = 0.0) const;

  private:
    /// \brief Вычисление значения без параметров
    double Value() const;

  protected:
    /// \brief Производит проверку корректности функции и установку соответствующего статуса.
    virtual void CheckFunction()
    {}

  private:
    RandomDistrPtr myRandom; ///< Генератор случайной величины

    friend class Function::Factory;
  };

}

#endif // _FUNCTION_RANDOM_H_
