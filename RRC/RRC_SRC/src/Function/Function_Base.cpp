/// \file   Function_Base.cpp
/// \author Artem A, Zhidkov
/// \date   04.01.2015
/// \brief  Реализация методов базового класса для математических функций

#include <Function_Base.h>

void Function::Base::AddVariable(const std::string& theVarName)
{
  myVariables.push_back(theVarName);
  CheckFunction();
}

void Function::Base::SetVariables(const std::vector<std::string>& theVariables)
{
  myVariables = theVariables;
  CheckFunction();
}

