/// \file   Function_Random.cpp
/// \author Artem A, Zhidkov
/// \date   09.01.2015
/// \brief  Реализация класса функций, задающих случайную величину

#include <Function_Random.h>

const Function::Status& Function::Random::Parse(const std::istream& theData)
{
  /// \todo Реализовать Function::Random::Parse(const std::istream& theData)
  return Status();
}

const Function::Status& Function::Random::Parse(const std::string& theData)
{
  /// \todo Реализовать Function::Random::Parse(const std::string& theData)
  return Status();
}

double Function::Random::Value(const VarValueMap& theParameters) const
{
  return Value();
}

double Function::Random::Value(const std::vector<double>& theParameters) const
{
  return Value();
}

double Function::Random::Value(const double& theX,
                               const double& theY,
                               const double& theZ) const
{
  return Value();
}

double Function::Random::Value() const
{
  return myRandom->Value();
}
