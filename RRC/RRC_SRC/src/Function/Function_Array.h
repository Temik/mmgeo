/// \file   Function_Array.h
/// \author Artem A, Zhidkov
/// \date   09.01.2015
/// \brief  Описание класса функций, заданных таблично

#ifndef _FUNCTION_ARRAY_H_
#define _FUNCTION_ARRAY_H_

#include <Function_Base.h>

namespace Function
{
  /** \class Function::Array
   *  \ingroup Math
   *  \brief Класс функций, заданных таблично
   *
   *  Определяется многомерной таблицей значений:
   *    A_11 A_12 ... A_1N
   *    A_21 A_22 ... A_2N
   *              ...
   *    A_M1 A_M2 ... A_MN
   *
   *  Для корректного вычисления значения необходимо задание размеров таблицы
   *  по каждому направлению и размерности таблицы.
   *
   *  Значения функции вычисляются только для целочисленных параметров.
   *  Интерполяция на вещественные аргументы не производится.
   */
  class Array : public Base
  {
  protected:
    Array() : Base() {}

  public:
    FUNCTION_EXPORT virtual ~Array() {}

    /** \brief Установка размера таблицы вдоль соответствующей переменной
     *  \param[in] theDimIndex  индекс переменной (начинается с 1)
     *  \param[in] theSize      размер таблицы
     */
    FUNCTION_EXPORT void SetDimension(int theDimIndex, size_t theSize);
    /** \brief Установка размера таблицы вдоль соответствующей переменной
     *  \param[in] theVarName  имя переменной
     *  \param[in] theSize     размер таблицы
     */
    FUNCTION_EXPORT void SetDimension(const std::string& theVarName, size_t theSize);
    /** \brief Установка размеров таблицы
     *  \param[in] theSizes  список размеров таблицы по каждой переменной
     */
    FUNCTION_EXPORT void SetDimensions(const std::vector<size_t>& theSizes);

    /** \brief Осуществляет разбор входного потока, определяющего структуру функции
     *  \param[in] theData  входной поток
     *  \return Статус корректности данных во входном потоке
     *
     *  Входной поток задаётся набором чисел, разделённых пробелами.
     */
    FUNCTION_EXPORT virtual const Function::Status& Parse(const std::istream& theData);
    /** \brief Осуществляет разбор входной строки, определяющей функцию
     *  \param[in] theData  входная строка
     *  \return Статус корректности заданной строки
     *
     *  Строка задаётся набором чисел, разделённых пробелами.
     */
    FUNCTION_EXPORT virtual const Function::Status& Parse(const std::string& theData);

    /** \brief Вычисление значения функции в заданной точке
     *  \param[in] theParameters значения аргументов функции
     *  \return Значение функции при заданных параметрах
     */
    FUNCTION_EXPORT virtual double Value(const VarValueMap& theParameters) const;
    /** \brief Вычисление значения функции в заданной точке
     *  \param[in] theParameters значения аргументов функции
     *  \return Значение функции при заданных параметрах
     *
     *  Последовательность параметров должна соответствовать последовательности аргументов функции.
     */
    FUNCTION_EXPORT virtual double Value(const std::vector<double>& theParameters) const;
    /** \brief Вычисление значения функции в заданной точке
     *  \param[in] theX  первый аргумент функции
     *  \param[in] theY  второй аргумент функции
     *  \param[in] theZ  третий аргумент функции
     *  \return Значение функции при заданных параметрах
     *
     *  Последовательность параметров должна соответствовать последовательности аргументов функции.
     */
    FUNCTION_EXPORT virtual double Value(const double& theX,
                                         const double& theY = 0.0,
                                         const double& theZ = 0.0) const;

  protected:
    /// \brief Производит проверку корректности функции и установку соответствующего статуса.
    virtual void CheckFunction();

  protected:
    std::vector<size_t> mySizes;  ///< Размеры таблицы вдоль каждой переменной
    std::vector<double> myValues; ///< Значения функции (задаются построчно: A_11 ... A_1N A_21 ...)

    friend class Function::Factory;
  };

}

#endif // _FUNCTION_ARRAY_H_
