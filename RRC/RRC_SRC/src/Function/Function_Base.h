/// \file   Function_Base.h
/// \author Artem A, Zhidkov
/// \date   04.01.2015
/// \brief  Описание базового класса для математических функций

#ifndef _FUNCTION_BASE_H_
#define _FUNCTION_BASE_H_

#include <Function.h>

#include <istream>
#include <map>
#include <string>
#include <vector>

#include <boost/shared_ptr.hpp>


/// \brief Описывает соответствие имени переменной и её значения
typedef std::map<std::string, double> VarValueMap;

namespace Function
{
  class Factory;

  /** \class Function::Base
   *  \ingroup Math
   *  \brief Базовый класс для аналитических функций, сеточных функций и функций, заданных таблично
   *
   *  Предоставляет интерфейс для вычисления значения функции нескольких переменных в заданной точке.
   */
  class Base
  {
  protected:
    Base() {}

  public:
    FUNCTION_EXPORT virtual ~Base() {}

    /** \brief Осуществляет разбор входного потока, определяющего структуру функции
     *  \param[in] theData  входной поток
     *  \return Статус корректности данных во входном потоке
     */
    FUNCTION_EXPORT virtual const Function::Status& Parse(const std::istream& theData) = 0;
    /** \brief Осуществляет разбор входной строки, определяющей функцию
     *  \param[in] theData  входная строка
     *  \return Статус корректности заданной строки
     */
    FUNCTION_EXPORT virtual const Function::Status& Parse(const std::string& theData) = 0;

    /** \brief Добавление имени переменной в конец списка аргументов функции
     *  \param[in] theVarName имя добавляемой переменной
     */
    FUNCTION_EXPORT virtual void AddVariable(const std::string& theVarName);
    /** \brief Установка имён переменных (аргументов функции)
     *  \param[in] theVariables список имён переменных
     */
    FUNCTION_EXPORT virtual void SetVariables(const std::vector<std::string>& theVariables);

    /** \brief Вычисление значения функции в заданной точке
     *  \param[in] theParameters значения аргументов функции
     *  \return Значение функции при заданных параметрах
     */
    FUNCTION_EXPORT virtual double Value(const VarValueMap& theParameters) const = 0;
    /** \brief Вычисление значения функции в заданной точке
     *  \param[in] theParameters значения аргументов функции
     *  \return Значение функции при заданных параметрах
     *
     *  Последовательность параметров должна соответствовать последовательности аргументов функции.
     */
    FUNCTION_EXPORT virtual double Value(const std::vector<double>& theParameters) const = 0;
    /** \brief Вычисление значения функции в заданной точке
     *  \param[in] theX  первый аргумент функции
     *  \param[in] theY  второй аргумент функции
     *  \param[in] theZ  третий аргумент функции
     *  \return Значение функции при заданных параметрах
     *
     *  Последовательность параметров должна соответствовать последовательности аргументов функции.
     */
    FUNCTION_EXPORT virtual double Value(const double& theX,
                                         const double& theY = 0.0,
                                         const double& theZ = 0.0) const = 0;

    /// \brief Корректность данной функции
    FUNCTION_EXPORT const Function::Status& Status() const
    { return myStatus; }

  protected:
    /// \brief Производит проверку корректности функции и установку соответствующего статуса.
    ///        Используется для явного вызова проверки из производных классов.
    virtual void CheckFunction() = 0;

  private:
    Function::Status         myStatus;    ///< Корректность задания функции

    std::vector<std::string> myVariables; ///< Список имён переменных, от которых зависит функция

    friend class Function::Factory;
  };

}

typedef boost::shared_ptr<Function::Base> FunctionPtr;

#endif // _FUNCTION_BASE_H_
