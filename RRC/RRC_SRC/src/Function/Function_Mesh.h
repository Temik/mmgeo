/// \file   Function_Mesh.h
/// \author Artem A, Zhidkov
/// \date   09.01.2015
/// \brief  Описание базового класса сеточных функций

#ifndef _FUNCTION_MESH_H_
#define _FUNCTION_MESH_H_

#include <Function_Base.h>

namespace Function
{
  /** \class Function::Mesh
   *  \ingroup Math
   *  \brief Базовый класс для описания сеточных функций
   *
   *  Функция определяется в конечном числе точек.
   *  В произвольной точке значение функции вычисляется по интерполяционным формулам,
   *  определяемым соответствующим дочерним классом.
   */
  class Mesh : public Base
  {
  protected:
    Mesh() : Base() {}

  public:
    FUNCTION_EXPORT virtual ~Mesh() {}

    /** \brief Осуществляет разбор входного потока, определяющего структуру функции
     *  \param[in] theData  входной поток
     *  \return Статус корректности данных во входном потоке
     */
    FUNCTION_EXPORT virtual const Function::Status& Parse(const std::istream& theData);
    /** \brief Осуществляет разбор входной строки, определяющей функцию
     *  \param[in] theData  входная строка
     *  \return Статус корректности заданной строки
     */
    FUNCTION_EXPORT virtual const Function::Status& Parse(const std::string& theData);
  };

}

#endif // _FUNCTION_MESH_H_
