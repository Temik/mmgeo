/// \file   Function_Mesh.cpp
/// \author Artem A, Zhidkov
/// \date   09.01.2015
/// \brief  Реализация базового класса сеточных функций

#include <Function_Mesh.h>

const Function::Status& Function::Mesh::Parse(const std::istream& theData)
{
  /// \todo Реализовать Function::Mesh::Parse(const std::istream& theData)
  return Status();
}

const Function::Status& Function::Mesh::Parse(const std::string& theData)
{
  /// \todo Реализовать Function::Mesh::Parse(const std::string& theData)
  return Status();
}
