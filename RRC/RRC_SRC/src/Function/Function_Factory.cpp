/// \file   Function_Factory.cpp
/// \author Artem A, Zhidkov
/// \date   04.01.2015
/// \brief  Реализация фабрики математических функций

#include <Function_Factory.h>
#include <Function_Analytic.h>
#include <Function_Array.h>
#include <Function_IndexedArray.h>
#include <Function_Random.h>

#include <fstream>
#include <sstream>

FunctionPtr Function::Factory::CreateFunction(const Function::Type& theFuncType,
                                              const std::istream&   theData)
{
  FunctionPtr aResult;
  switch (theFuncType)
  {
  case FUNC_ANALYTIC:
    aResult = FunctionPtr(new Function::Analytic);
    break;
  case FUNC_MESH:
    /// \todo Реализовать различные способы интерполяции сеточных функций
    break;
  case FUNC_ARRAY:
    aResult = FunctionPtr(new Function::Array);
    break;
  case FUNC_ARRAY_INDEXED:
    aResult = FunctionPtr(new Function::IndexedArray);
    break;
  case FUNC_DISTRIBUTION:
    aResult = FunctionPtr(new Function::Random);
    break;
  }
  aResult->Parse(theData);
  return aResult;
}

FunctionPtr Function::Factory::CreateFunction(const Function::Type& theFuncType,
                                              const std::string&    theData)
{
  std::istringstream aStrReader(theData, std::istringstream::in);
  return CreateFunction(theFuncType, aStrReader);
}

FunctionPtr Function::Factory::CreateFunctionFromFile(const Function::Type& theFuncType,
                                                      const std::string&    theFileName)
{
  std::ifstream aFile(theFileName.c_str(), std::fstream::in);
  if (!aFile.is_open())
    return FunctionPtr();
  return CreateFunction(theFuncType, aFile);
}
