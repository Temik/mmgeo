/// \file   Function_Factory.h
/// \author Artem A, Zhidkov
/// \date   04.01.2015
/// \brief  Описание фабрики математических функций

#ifndef _FUNCTION_FACTORY_H_
#define _FUNCTION_FACTORY_H_

#include <Function.h>
#include <Function_Base.h>

namespace Function
{
  /** \class Function::Factory
   *  \ingroup Math
   *  \brief Фабрика математических функций
   *
   *  Позволяет создавать новые объекты-функции на основе типа данной функции
   *  и входного потока, описывающего её содержимое.
   */
  class Factory
  {
  public:
    /** \brief Создание функции на основе её типа и входного потока
     *  \param[in] theFuncType тип функции
     *  \param[in] theData     входной поток, описывающий содержимое функции
     *  \return Указатель на созданный объект
     */
    FUNCTION_EXPORT static FunctionPtr CreateFunction(const Function::Type& theFuncType,
                                                      const std::istream&   theData);

    /** \brief Создание функции на основе её типа и её строкового представления
     *  \param[in] theFuncType тип функции
     *  \param[in] theData     содержимое функции, заданное в виде строки
     *  \return Указатель на созданный объект
     */
    FUNCTION_EXPORT static FunctionPtr CreateFunction(const Function::Type& theFuncType,
                                                      const std::string&    theData);

    /** \brief Чтение функции из файла
     *  \param[in] theFuncType тип функции
     *  \param[in] theFileName имя файла, содержащего функцию
     *  \return Указатель на созданный объект
     */
    FUNCTION_EXPORT static FunctionPtr CreateFunctionFromFile(const Function::Type& theFuncType,
                                                              const std::string&    theFileName);
  };

}

#endif // _FUNCTION_FACTORY_H_
