/// \file   Function_Array.cpp
/// \author Artem A, Zhidkov
/// \date   09.01.2015
/// \brief  Реализация класса функций, заданных таблично

#include <Function_Array.h>

const Function::Status& Function::Array::Parse(const std::istream& theData)
{
  /// \todo Реализовать Function::Array::Parse(const std::istream& theData)
  return Status();
}

const Function::Status& Function::Array::Parse(const std::string& theData)
{
  /// \todo Реализовать Function::Array::Parse(const std::string& theData)
  return Status();
}

double Function::Array::Value(const VarValueMap& theParameters) const
{
  /// \todo Реализовать Function::Array::Value(const VarValueMap& theParameters) const
  return 0.0;
}

double Function::Array::Value(const std::vector<double>& theParameters) const
{
  /// \todo Реализовать Function::Array::Value(const std::vector<double>& theParameters) const
  return 0.0;
}

double Function::Array::Value(const double& theX,
                              const double& theY,
                              const double& theZ) const
{
  /// \todo Реализовать Function::Array::Value(theX, theY, theZ) const
  return 0.0;
}

void Function::Array::CheckFunction()
{
  /// \todo Реализовать Function::Array::CheckFunction()
}
