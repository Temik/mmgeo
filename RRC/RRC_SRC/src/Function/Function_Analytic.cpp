/// \file   Function_Analytic.cpp
/// \author Artem A, Zhidkov
/// \date   09.01.2015
/// \brief  Реализация класса аналитических функций

#include <Function_Analytic.h>

const Function::Status& Function::Analytic::Parse(const std::istream& theData)
{
  /// \todo Реализовать Function::Analytic::Parse(const std::istream& theData)
  return Status();
}

const Function::Status& Function::Analytic::Parse(const std::string& theData)
{
  /// \todo Реализовать Function::Analytic::Parse(const std::string& theData)
  return Status();
}

double Function::Analytic::Value(const VarValueMap& theParameters) const
{
  /// \todo Реализовать Function::Analytic::Value(const VarValueMap& theParameters) const
  return 0.0;
}

double Function::Analytic::Value(const std::vector<double>& theParameters) const
{
  /// \todo Реализовать Function::Analytic::Value(const std::vector<double>& theParameters) const
  return 0.0;
}

double Function::Analytic::Value(const double& theX,
                                 const double& theY,
                                 const double& theZ) const
{
  /// \todo Реализовать Function::Analytic::Value(theX, theY, theZ) const
  return 0.0;
}

void Function::Analytic::CheckFunction()
{
  /// \todo Реализовать Function::Analytic::CheckFunction()
}
