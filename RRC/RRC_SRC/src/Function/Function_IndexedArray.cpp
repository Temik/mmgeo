/// \file   Function_IndexedArray.cpp
/// \author Artem A, Zhidkov
/// \date   09.01.2015
/// \brief  Реализация класса функций, заданных таблично

#include <Function_IndexedArray.h>

const Function::Status& Function::IndexedArray::Parse(const std::istream& theData)
{
  /// \todo Реализовать Function::IndexedArray::Parse(const std::istream& theData)
  return Status();
}

const Function::Status& Function::IndexedArray::Parse(const std::string& theData)
{
  /// \todo Реализовать Function::IndexedArray::Parse(const std::string& theData)
  return Status();
}

double Function::IndexedArray::Value(const VarValueMap& theParameters) const
{
  /// \todo Реализовать Function::IndexedArray::Value(const VarValueMap& theParameters) const
  return 0.0;
}

double Function::IndexedArray::Value(const std::vector<double>& theParameters) const
{
  /// \todo Реализовать Function::IndexedArray::Value(const std::vector<double>& theParameters) const
  return 0.0;
}

double Function::IndexedArray::Value(const double& theX,
                                     const double& theY,
                                     const double& theZ) const
{
  /// \todo Реализовать Function::IndexedArray::Value(theX, theY, theZ) const
  return 0.0;
}

void Function::IndexedArray::CheckFunction()
{
  /// \todo Реализовать Function::IndexedArray::CheckFunction()
}
