/// \file   Function_Analytic.h
/// \author Artem A, Zhidkov
/// \date   09.01.2015
/// \brief  Описание класса аналитических функций

#ifndef _FUNCTION_ANALYTIC_H_
#define _FUNCTION_ANALYTIC_H_

#include <Function_Base.h>

namespace Function
{
  /** \class Function::Analytic
   *  \ingroup Math
   *  \brief Класс для описания аналитических функций
   */
  class Analytic : public Base
  {
  protected:
    Analytic() : Base() {}

  public:
    FUNCTION_EXPORT virtual ~Analytic() {}

    /** \brief Осуществляет разбор входного потока, определяющего структуру функции
     *  \param[in] theData  входной поток
     *  \return Статус корректности данных во входном потоке
     */
    FUNCTION_EXPORT virtual const Function::Status& Parse(const std::istream& theData);
    /** \brief Осуществляет разбор входной строки, определяющей функцию
     *  \param[in] theData  входная строка
     *  \return Статус корректности заданной строки
     */
    FUNCTION_EXPORT virtual const Function::Status& Parse(const std::string& theData);

    /** \brief Вычисление значения функции в заданной точке
     *  \param[in] theParameters значения аргументов функции
     *  \return Значение функции при заданных параметрах
     */
    FUNCTION_EXPORT virtual double Value(const VarValueMap& theParameters) const;
    /** \brief Вычисление значения функции в заданной точке
     *  \param[in] theParameters значения аргументов функции
     *  \return Значение функции при заданных параметрах
     *
     *  Последовательность параметров должна соответствовать последовательности аргументов функции.
     */
    FUNCTION_EXPORT virtual double Value(const std::vector<double>& theParameters) const;
    /** \brief Вычисление значения функции в заданной точке
     *  \param[in] theX  первый аргумент функции
     *  \param[in] theY  второй аргумент функции
     *  \param[in] theZ  третий аргумент функции
     *  \return Значение функции при заданных параметрах
     *
     *  Последовательность параметров должна соответствовать последовательности аргументов функции.
     */
    FUNCTION_EXPORT virtual double Value(const double& theX,
                                         const double& theY = 0.0,
                                         const double& theZ = 0.0) const;

  protected:
    /// \brief Производит проверку корректности функции и установку соответствующего статуса.
    virtual void CheckFunction();

  private:

    friend class Function::Factory;
  };

}

#endif // _FUNCTION_ANALYTIC_H_
