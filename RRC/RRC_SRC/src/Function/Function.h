/// \file   Function.h
/// \author Artem A, Zhidkov
/// \date   04.01.2015
/// \brief  Основные определения модуля Function

#ifndef _FUNCTION_H_
#define _FUNCTION_H_

#if defined WIN32
# if defined Function_EXPORTS
#   define FUNCTION_EXPORT __declspec(dllexport)
# else
#   define FUNCTION_EXPORT __declspec(dllimport)
# endif
#else
# define FUNCTION_EXPORT
#endif


namespace Function
{
  /// \brief Показывает ошибки в определении функции
  enum Status
  {
    STATUS_FUNC_OK ///< Функция задана корректно
  };


  /// \brief Тип функции
  enum Type
  {
    FUNC_ANALYTIC,      ///< аналитическая функция
    FUNC_MESH,          ///< сеточная функция
    FUNC_ARRAY,         ///< функция, заданная в виде многомерной таблицы
    FUNC_ARRAY_INDEXED, ///< функция в виде таблицы, положение значения в которой задано индексами ячейки
    FUNC_DISTRIBUTION,  ///< функция, заданная плотностью вероятности
    FUNC_UNKNOWN        ///< неизвестный тип функции
  };
}

#endif // _FUNCTION_H_
