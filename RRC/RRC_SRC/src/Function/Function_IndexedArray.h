/// \file   Function_IndexedArray.h
/// \author Artem A, Zhidkov
/// \date   09.01.2015
/// \brief  Описание класса функций, заданных таблично

#ifndef _FUNCTION_INDEXEDARRAY_H_
#define _FUNCTION_INDEXEDARRAY_H_

#include <Function_Array.h>

namespace Function
{
  /** \class Function::IndexedArray
   *  \ingroup Math
   *  \brief Класс функций, заданных значениями и их индексами в таблице
   *
   *  Значения могут заполнять не всю таблицу. Индексы значений могут быть отрицательными.
   */
  class IndexedArray : public Array
  {
  protected:
    IndexedArray() : Array() {}

  public:
    FUNCTION_EXPORT virtual ~IndexedArray() {}

    /** \brief Осуществляет разбор входного потока, определяющего структуру функции
     *  \param[in] theData  входной поток
     *  \return Статус корректности данных во входном потоке
     */
    FUNCTION_EXPORT virtual const Function::Status& Parse(const std::istream& theData);
    /** \brief Осуществляет разбор входной строки, определяющей функцию
     *  \param[in] theData  входная строка
     *  \return Статус корректности заданной строки
     */
    FUNCTION_EXPORT virtual const Function::Status& Parse(const std::string& theData);

    /** \brief Вычисление значения функции в заданной точке
     *  \param[in] theParameters значения аргументов функции
     *  \return Значение функции при заданных параметрах
     */
    FUNCTION_EXPORT virtual double Value(const VarValueMap& theParameters) const;
    /** \brief Вычисление значения функции в заданной точке
     *  \param[in] theParameters значения аргументов функции
     *  \return Значение функции при заданных параметрах
     *
     *  Последовательность параметров должна соответствовать последовательности аргументов функции.
     */
    FUNCTION_EXPORT virtual double Value(const std::vector<double>& theParameters) const;
    /** \brief Вычисление значения функции в заданной точке
     *  \param[in] theX  первый аргумент функции
     *  \param[in] theY  второй аргумент функции
     *  \param[in] theZ  третий аргумент функции
     *  \return Значение функции при заданных параметрах
     *
     *  Последовательность параметров должна соответствовать последовательности аргументов функции.
     */
    FUNCTION_EXPORT virtual double Value(const double& theX,
                                         const double& theY = 0.0,
                                         const double& theZ = 0.0) const;

  protected:
    /// \brief Производит проверку корректности функции и установку соответствующего статуса.
    virtual void CheckFunction();

  private:
    std::vector<bool> myIsAssigned; ///< Показывает, какое из значений myValues определено. Размер совпадает с размером myValues

    std::vector<int>  myMinIndexes; ///< Минимальный индекс ячейки таблицы по каждой переменной
    std::vector<int>  myMaxIndexes; ///< Максимальный индекс ячейки таблицы по каждой переменной

    friend class Function::Factory;
  };

}

#endif // _FUNCTION_INDEXEDARRAY_H_
