/// \file   DataModel.h
/// \author Artem A, Zhidkov
/// \date   01.02.2015
/// \brief  Основные определения модуля DataModel

#ifndef _DATAMODEL_H_
#define _DATAMODEL_H_

#if defined WIN32
# if defined DataModel_EXPORTS
#   define DATAMODEL_EXPORT __declspec(dllexport)
# else
#   define DATAMODEL_EXPORT __declspec(dllimport)
# endif
#else
# define DATAMODEL_EXPORT
#endif

/// \brief Уникальный идентификатор объекта
typedef unsigned long UniqueID;

#endif // _DATAMODEL_H_
