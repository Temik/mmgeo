/** \file   RRCApp.cpp
 *  \author Artem A. Zhidkov
 *  \date   Dec 8, 2014
 *
 *  \brief Main file of application
 */

#include <RRC_Configure.h>

#include <Exchange_ConfigReader.h>

#include <boost/program_options.hpp>
namespace BoostPO = boost::program_options;

#include <iostream>
#include <sstream>

int main(int argc, char* argv[])
{
  std::ostringstream aHeader;
  aHeader << "\nRRCApp (Relay-Race Conductivity), version " << RRC_VERSION_MAJOR << "." << RRC_VERSION_MINOR
          << "\nList of supported options";

  // Читаем параметры командной строки
  BoostPO::options_description anOptions(aHeader.str());
  anOptions.add_options()
    ("help,h", "display this help message")
    ("config,c", BoostPO::value<std::string>(), "the name of config-file")
    ("create-config", BoostPO::value<std::string>(),
        "the directory, where a template of config-file should be created");

  BoostPO::variables_map aVarMap;
  BoostPO::store(BoostPO::parse_command_line(argc, argv, anOptions), aVarMap);
  BoostPO::notify(aVarMap);

  if (aVarMap.size() == 0 || aVarMap.count("help"))
  {
    std::cout << anOptions << std::endl;
    return 0;
  }

  Exchange::ConfigReader* aConfigReader = Exchange::ConfigReader::GetInstance();

  if (aVarMap.count("create-config"))
  {
    std::string aPathToCreateConfig = aVarMap["create-config"].as<std::string>();
    if (aConfigReader->CopyTemplateTo(aPathToCreateConfig) != Exchange::STATUS_OK)
      std::cout << "Error: " << aConfigReader->StatusDescription() << std::endl;
    else
      std::cout << "Template of config-file was copied to " << aPathToCreateConfig << std::endl;
    return 0;
  }

  if (aVarMap.count("config"))
  {
    std::string aConfigFile = aVarMap["config"].as<std::string>();
    if (aConfigReader->ProcessConfigFile(aConfigFile) != Exchange::STATUS_OK)
    {
      std::cout << "Error: " << aConfigReader->StatusDescription() << std::endl;
      return aConfigReader->Status();
    }
    return 0;
  }

  std::cout << "Error: Wrong option is used" << std::endl;
  return -1;
}
