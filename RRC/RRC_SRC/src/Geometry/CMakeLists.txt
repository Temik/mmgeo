# File:   RRC_SRC/src/Geometry/CMakeLists.txt
# Author: Artem A. Zhidkov
# Date:   Mar 12, 2015

SET(GEOMETRY_SOURCES
    Geometry_Point3D.cpp
    Geometry_Vector3D.cpp
)

SET(GEOMETRY_HEADERS
    Geometry.h
    Geometry_Point3D.h
    Geometry_Vector3D.h
)

ADD_DEFINITIONS(-DGeometry_EXPORTS)

ADD_LIBRARY(Geometry SHARED ${GEOMETRY_SOURCES} ${GEOMETRY_HEADERS})

INSTALL(TARGETS Geometry DESTINATION ${RRC_BIN})
INSTALL(FILES ${GEOMETRY_HEADERS} DESTINATION ${RRC_INC})
