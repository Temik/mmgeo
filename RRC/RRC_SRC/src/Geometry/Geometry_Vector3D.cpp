/// \ file   Geometry_Vector.h
/// \author  Ishchenko E.A.
/// \brief   Реализация класса для работы с векторами

#include <Geometry_Vector3D.h>
#include <cmath>

using namespace Geometry;

Vector3D::Vector3D(double theX, double theY, double theZ)
{
  SetCoords(theX, theY, theZ);
}


void Vector3D::SetCoords(double theX, double theY, double theZ)
{
  myX = theX;
  myY = theY;
  myZ = theZ;
}


void Vector3D::GetCoords(double& theX, double& theY, double& theZ) const
{
  theX = myX;
  theY = myY;
  theZ = myZ;
}


void Vector3D::Normalize()
{
  double length = Length();
  if (fabs(length) < 1.e-20)
    return;
  myX /= length;
  myY /= length;
  myZ /= length;
}

double Vector3D::SquareLength() const
{
  return myX * myX + myY * myY + myZ * myZ;
}

double Vector3D::Length() const
{
  // возвращаем корень из квадрата длины
  return sqrt(SquareLength());
}


Vector3D Vector3D::operator +(const Vector3D &theVector) const
{
  return Vector3D(myX + theVector.myX,
                  myY + theVector.myY,
                  myZ + theVector.myZ);
}

Vector3D Vector3D::operator* (double theCoeff) const
{
  return Vector3D(myX * theCoeff,
                  myY * theCoeff,
                  myZ * theCoeff);
}

Vector3D Geometry::operator *(double theCoeff, const Vector3D& theVector)
{
  return Vector3D(theCoeff * theVector.myX,
                  theCoeff * theVector.myY,
                  theCoeff * theVector.myZ);
}

double Geometry::ScalarProduct(const Vector3D& theVec1, const Vector3D& theVec2)
{
  return theVec1.myX * theVec2.myX +
         theVec1.myY * theVec2.myY +
         theVec1.myZ * theVec2.myZ;
}

Vector3D Geometry::VectorProduct(const Vector3D& theVec1, const Vector3D& theVec2)
{
  return Vector3D(theVec1.myY * theVec2.myZ - theVec1.myZ * theVec2.myY,
                  theVec1.myZ * theVec2.myX - theVec1.myX * theVec2.myZ,
                  theVec1.myX * theVec2.myY - theVec1.myY * theVec2.myX);
}
