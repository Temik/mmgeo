/// \file   Geometry_Point3D.h
/// \author Svetlana Lavrova
/// \date   19.03.15 
/// \brief  Класс, описывающий методы работы с точкми в трёхмерном пространстве

#ifndef _GEOMETRY_POINT3D_H_
#define _GEOMETRY_POINT3D_H_

#include <Geometry.h>

namespace Geometry
{
  class Point3D
  {
  public:
    GEOMETRY_EXPORT Point3D() : myX(0), myY(0), myZ(0) {}
    GEOMETRY_EXPORT Point3D(double theX, double theY, double theZ);
    GEOMETRY_EXPORT virtual ~Point3D() {}
    /// \brief Изменяет координаты точки
    GEOMETRY_EXPORT void SetCoords(double theX, double theY, double theZ);
    /// \brief Возвращает координаты точки
    GEOMETRY_EXPORT void GetCoords(double &theX, double &theY, double &theZ) const;
    /// \brief Вычисление расстояния между точками
    GEOMETRY_EXPORT double Distance(const Point3D& thePoint) const;
    /// \brief Вычисление квадрата расстояния между точками
    GEOMETRY_EXPORT double SquareDistance(const Point3D& thePoint) const;

  private:
    double myX;
    double myY;
    double myZ;
  };
}

#endif // _GEOMETRY_POINT3D_H_

