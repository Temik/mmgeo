/// \file   Geometry.h
/// \author Artem A, Zhidkov
/// \date   12.03.2015
/// \brief  Основные определения модуля Geometry

#ifndef _GEOMETRY_H_
#define _GEOMETRY_H_

#if defined WIN32
# if defined Geometry_EXPORTS
#   define GEOMETRY_EXPORT __declspec(dllexport)
# else
#   define GEOMETRY_EXPORT __declspec(dllimport)
# endif
#else
# define GEOMETRY_EXPORT
#endif

#endif // _GEOMETRY_H_
