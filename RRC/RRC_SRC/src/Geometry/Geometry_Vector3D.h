/// \ file   Geometry_Vector.h
/// \author  Ishchenko E.A.
/// \brief   Класс для работы с векторами

#ifndef _GEOMETRY_VECTOR_H_
#define _GEOMETRY_VECTOR_H_

#include <Geometry.h>

namespace Geometry
{
  class Vector3D
  {
  private:
    double myX;
    double myY;
    double myZ;
  public:
    GEOMETRY_EXPORT Vector3D(): myX(0), myY(0), myZ(0) {}
    GEOMETRY_EXPORT Vector3D(double theX, double theY, double theZ);

    /// \brief Установка (изменение) координат
    GEOMETRY_EXPORT void SetCoords(double theX, double theY, double theZ);
    /// \brief Полу чение координат
    GEOMETRY_EXPORT void GetCoords(double& theX, double& theY, double& theZ) const;
    /// \brief Нормировка вектора
    GEOMETRY_EXPORT void Normalize();
    
    /// \brief Вычисление длины
    GEOMETRY_EXPORT double Length() const;
    /// \brief Вычисление квадрата длины
    GEOMETRY_EXPORT double SquareLength() const;

    /// \brief Сложение векторов
    GEOMETRY_EXPORT Vector3D operator +(const Vector3D& theVector) const;
    /// \brief Умножение вектора на число
    GEOMETRY_EXPORT Vector3D operator *(double theCoeff) const;

    friend GEOMETRY_EXPORT Vector3D operator *(double, const Vector3D&);
    friend GEOMETRY_EXPORT double   ScalarProduct(const Vector3D&, const Vector3D&);
    friend GEOMETRY_EXPORT Vector3D VectorProduct(const Vector3D&, const Vector3D&);
  };

  /// \brief Умножение числа на вектор
  GEOMETRY_EXPORT Vector3D operator *(double, const Vector3D&);
  /// \brief Скалярное произведение векторов
  GEOMETRY_EXPORT double ScalarProduct(const Vector3D&, const Vector3D&);
  /// \brief Векторное произведение векторов
  GEOMETRY_EXPORT Vector3D VectorProduct(const Vector3D&, const Vector3D&);
}

#endif // _GEOMETRY_VECTOR_H_
