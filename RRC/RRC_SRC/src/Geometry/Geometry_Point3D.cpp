/// \file   Geometry_Point3D.h
/// \author Svetlana Lavrova
/// \date   19.03.15
/// \brief  Реализация методов работы с точками

#include <Geometry_Point3D.h>
#include <cmath>

using namespace Geometry;

Point3D::Point3D(double theX, double theY, double theZ)
{
  SetCoords(theX, theY, theZ);
}

void Point3D::SetCoords(double theX, double theY, double theZ)
{
  myX = theX;
  myY = theY;
  myZ = theZ;
}

void Point3D::GetCoords(double &theX, double &theY, double &theZ) const
{
  theX = myX;
  theY = myY;
  theZ = myZ;
}

double Point3D::Distance(const Point3D& thePoint) const
{
  return sqrt(SquareDistance(thePoint));
}

double Point3D::SquareDistance(const Point3D& thePoint) const
{
  return (thePoint.myX - myX) * (thePoint.myX - myX) +
         (thePoint.myY - myY) * (thePoint.myY - myY) +
         (thePoint.myZ - myZ) * (thePoint.myZ - myZ);
}
