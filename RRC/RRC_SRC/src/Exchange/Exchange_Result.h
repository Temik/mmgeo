/// \file   Exchange_Result.h
/// \author Artem A, Zhidkov
/// \date   31.01.2015
/// \brief  Описание объекта для хранения результатов расчёта

#ifndef _EXCHANGE_RESULT_H_
#define _EXCHANGE_RESULT_H_

#include <Exchange.h>

#include <DataModel.h>
#include <map>
#include <vector>

/// Порог, при превышении которого происходит запись информации в файл.
/// Составляет 1МБ
#define EXCHANGE_MEMORY_THRESHOLD 1048576

/** \brief Взаимнооднозначное соответствие между номером шага по времени и частицами,
 *         изменившими свои заряды. Частицы объединяются по кластерам.
 */
typedef std::map<unsigned long, std::vector< std::map<UniqueID, double> > > MapTimeData;

namespace Exchange
{
  /** \class Exchange::Result
   *  \ingroup Exchange
   *  \brief Класс для хранения результатов расчётов и записи их в файл
   *
   *  Данные могут подаваться в объект в произвольном порядке.
   *
   *  При накоплении некоторого количества информации происходит её запись в файл.
   *  Название файла для записи берётся из конфигурационного файла.
   *
   *  Синглтон.
   */
  class Result
  {
  protected:
    Result(const std::string& theFileName);

  public:
    /// \brief Выдача указателя на экземпляр класса
    EXCHANGE_EXPORT static Result* GetInstance();

    /** \brief Устанавливает имя файла для вывода результата
     *  \param[in] theFileName  имя файла
     *  \return Корректность имени файла
     */
    EXCHANGE_EXPORT const Exchange::Status& SetExportFile(const std::string& theFileName);

    /** \brief Добавление результатов расчёта (или части результатов) на некотором шаге
     *  \param[in] theTimeStep  номер шага, на котором получены результаты
     *  \param[in] theCharges   список уникальных индексов частиц и зарядов, полученных ими на указанном шаге
     */
    EXCHANGE_EXPORT void AddResult(const unsigned long& theTimeStep,
                                   const std::map<UniqueID, double> theCharges);

    /// \brief Запускает полное сохранение всех добавленных результатов
    EXCHANGE_EXPORT void StoreResult();

    /// \brief Возвращает статус обработки конфигурационного файла
    EXCHANGE_EXPORT const Exchange::Status& Status() const
    { return myStatus; }

    /// \brief Возвращает строковое описание текущего статуса
    EXCHANGE_EXPORT std::string StatusDescription() const;

  protected:
    /** \brief Осуществляет сохранение в файл начальных состояний частиц
     *
     *  Информация о частицах берётся из модели данных.
     *  Сохраняются в файл все частицы, имеющиеся в модели данных.
     *  Сохранение результатов расчёта не начинается до тех пор,
     *  пока не будут сохранены все начальные данные
     */
    void StoreInitialData();

    /** \brief Осуществляет частичное сохранение данных в файл
     *
     *  Сохраняются все шаги, кроме последнего. Если в настоящее время хранится только один шаг,
     *  то есть количество данных для этого шага превышает пороговое значение, то сохраняются все данные.
     */
    void StorePartData();

    /** \brief Осуществляет частичное сохранение данных в файл
     *  \param[in] theFirst элементы, начиная с данного итератора должны быть сохранены
     *  \param[in] theLast  сохранение заканчивается элементов, предшествующим данному
     *
     *  Сохраняются элементы myData, начиная с theFirst, заканчивая элементом, предшествующим theLast.
     *  Сохранённые списки удаляются из myData.
     */
    void StorePartData(MapTimeData::iterator theFirst, MapTimeData::iterator theLast);

  private:
    static Result*   mySelf;           ///< Указатель на единственный экземпляр данного класса

    Exchange::Status myStatus;         ///< Определяет корректность введённого результата и сохранения данных в файле
    std::string      myStatusAddInfo;  ///< Дополнительная информация об ошибке

    std::string      myResFileName;    ///< Имя файла, в который будет записан результат
    bool             myInitialWritten; ///< Показывает, что начальные данные записаны в файл
    unsigned long    myStepWritten;    ///< Номер шага, который последним был записан в файл

    MapTimeData      myData;           ///< Списки частиц, изменивших заряды на соответствующем шаге
    size_t           myMemSize;        ///< Количество памяти, занимаемое структурой myData
  };

}

#endif // _EXCHANGE_RESULT_H_
