/// \file   Exchange_ConfigReader.h
/// \author Artem A. Zhidkov
/// \date   03.01.2015
/// \brief  Описание объекта для чтения конфигурационного файла

#ifndef _EXCHANGE_CONFIGREADER_H_
#define _EXCHANGE_CONFIGREADER_H_

#include <Exchange.h>

#include <Randomizer_Distribution.h>
#include <Function_Base.h>

#include <string>
#include <vector>

namespace Exchange
{
  /** \class Exchange::ConfigReader
   *  \ingroup Exchange
   *  \brief Объект, осуществляющий чтение конфигурационного файла задачи
   *
   *  Осуществляет чтение и хранение параметров задачи. Производит выдачу соответствующих параметров
   *  по запросу из-вне. Осуществляет копирование шаблона конфигурационного файла задачи.
   *
   *  Корректность чтения конфигурационного файла хранится в статусе.
   *
   *  Синглтон.
   */
  class ConfigReader
  {
  public:
    /// \brief Выдача указателя на экземпляр класса
    EXCHANGE_EXPORT static Exchange::ConfigReader* GetInstance();

    /** \brief Чтение и обработка конфигурационного файла
     *  \param[in] theFileName имя файла
     *  \return Статус корректности чтения файла (также может быть получен с помощью метода Status())
     */
    EXCHANGE_EXPORT const Exchange::Status& ProcessConfigFile(const std::string& theFileName);

    /** \brief Копирует шаблон конфигурационного файла в заданную директорию или файл
     *  \param[in] theOutDir имя файла или директории для копирования конфигурационного файла
     *  \return Статус корректности копирования файла
     */
    EXCHANGE_EXPORT const Exchange::Status& CopyTemplateTo(const std::string& theOutDir);

    /// \brief Возвращает статус обработки конфигурационного файла
    EXCHANGE_EXPORT const Exchange::Status& Status() const
    { return myStatus; }

    /// \brief Возвращает строковое описание текущего статуса
    EXCHANGE_EXPORT std::string StatusDescription() const;

    /** \brief Возвращает количество частиц вдоль каждой координатной оси
     *  \param[out] theNx количество частиц вдоль оси X
     *  \param[out] theNy количество частиц вдоль оси Y
     *  \param[out] theNz количество частиц вдоль оси Z
     */
    EXCHANGE_EXPORT void GridSizes(unsigned long& theNx, unsigned long& theNy, unsigned long& theNz) const
    { theNx = myGridSize[0]; theNy = myGridSize[1]; theNz = myGridSize[2]; }

    /// \brief Возвращает генератор размеров частиц
    EXCHANGE_EXPORT const RandomDistrPtr& ParticleSizes() const
    { return myParticleSizesDistr; }

    /// \brief Возвращает генератор расстояний между частицами
    EXCHANGE_EXPORT const RandomDistrPtr& ParticleDistances() const
    { return myParticleDistancesDistr; }

    /// \brief Возвращает вероятность возникновения/разрушения связи между частицами
    EXCHANGE_EXPORT const double& ConnectionRiseFall() const
    { return myConnectionProbability; }

    /** \brief Возвращает заряд частицы по её индексам в сетке
     *  \param[in] theIndX индекс частицы по оси X
     *  \param[in] theIndY индекс частицы по оси Y
     *  \param[in] theIndZ индекс частицы по оси Z
     *  \return Значение электрического заряда частицы
     */
    EXCHANGE_EXPORT double ParticleCharge(const long& theIndX, const long& theIndY, const long& theIndZ) const
    { return myChargesFunc->Value((double)theIndX, (double)theIndY, (double)theIndZ); }

    /** \brief Возвращает через параметры компоненты внешнего электрического поля
     *  \param[out] theX  внешнее поле вдоль оси X
     *  \param[out] theY  внешнее поле вдоль оси Y
     *  \param[out] theZ  внешнее поле вдоль оси Z
     */
    EXCHANGE_EXPORT void ExternalField(double& theX, double& theY, double& theZ) const
    { theX = myExtField[0]; theY = myExtField[1]; theZ = myExtField[2]; }

    /** \brief Возвращает имя файла, в который должен быть сохранён результат
     *  \return Имя и путь до файла с результатом расчёта
     */
    EXCHANGE_EXPORT const std::string& GetOutputFileName() const
    { return myOutputFile; }

  protected:
    ConfigReader();

  private:
    /** \brief Получает размеры сетки из параметров конфигурационного файла
     *  \param[in] theConfigVars параметры конфигурационного файла
     *
     *  Корректность работы метода может быть получена с помощью метода Status()
     */
    void SetGridSizes(const BoostPO::variables_map& theConfigVars);

    /** \brief Получает распределение размеров частиц и расстояний между ними
     *  \param[in] theConfigVars параметры конфигурационного файла
     *
     *  Корректность работы метода может быть получена с помощью метода Status()
     */
    void SetGridParameters(const BoostPO::variables_map& theConfigVars);

    /** \brief Получает распределение вероятности возникновения/разрушения связей между частицами
     *  \param[in] theConfigVars параметры конфигурационного файла
     *
     *  Корректность работы метода может быть получена с помощью метода Status()
     */
    void SetConnectionRiseFall(const BoostPO::variables_map& theConfigVars);

    /** \brief Получает заряды частиц в зависимости от их индексов
     *  \param[in] theConfigVars параметры конфигурационного файла
     *
     *  Корректность работы метода может быть получена с помощью метода Status()
     */
    void SetCharges(const BoostPO::variables_map& theConfigVars);

    /** \brief Получает компоненты внешнего электрического поля
     *  \param[in] theConfigVars параметры конфигурационного файла
     *
     *  Корректность работы метода может быть получена с помощью метода Status()
     */
    void SetExternalField(const BoostPO::variables_map& theConfigVars);

    /** \brief Получает имя результирующего файла
     *  \param[in] theConfigVars параметры конфигурационного файла
     *
     *  Корректность работы метода может быть получена с помощью метода Status()
     */
    void SetOutputFileName(const BoostPO::variables_map& theConfigVars);

  private:
    static ConfigReader* mySelf;      ///< Указатель на единственный экземпляр данного класса

    Exchange::Status myStatus;        ///< Статус чтения конфигурационного файла
    std::string      myStatusAddInfo; ///< Дополнительная информация об ошибке

    BoostFS::path    myConfigFile;    ///< Имя и путь до конфигурационного файла

    unsigned long    myGridSize[3];   ///< Количество частиц вдоль каждой оси

    RandomDistrPtr   myParticleSizesDistr;     ///< Генератор случайного распределения размеров частиц
    RandomDistrPtr   myParticleDistancesDistr; ///< Генератор случайного распределения расстояний между частицами
    double           myConnectionProbability;  ///< Вероятность возникновения/разрушения связей между частицами

    FunctionPtr      myChargesFunc;   ///< Функция, описывающая электрические заряды частиц

    double           myExtField[3];   ///< Значение внешнего электрического поля

    std::string      myOutputFile;    ///< Имя файла, в который будет сохранён результат
  };

}

#endif // _EXCHANGE_CONFIGREADER_H_
