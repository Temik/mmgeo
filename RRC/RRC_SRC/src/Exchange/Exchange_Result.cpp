/// \file   Exchange_Result.cpp
/// \author Artem A, Zhidkov
/// \date   31.01.2015
/// \brief  Реализация объекта для хранения результатов расчёта

#include <Exchange_Result.h>
#include <Exchange_ConfigReader.h>

Exchange::Result* Exchange::Result::mySelf = 0;

Exchange::Result* Exchange::Result::GetInstance()
{
  if (!mySelf)
  {
    Exchange::ConfigReader* aConfReader = Exchange::ConfigReader::GetInstance();
    mySelf = new Exchange::Result(aConfReader->GetOutputFileName());
    if (mySelf && mySelf->Status() != STATUS_OK)
      mySelf = 0;
  }
  return mySelf;
}

Exchange::Result::Result(const std::string& theFileName)
  : myInitialWritten(false),
    myStepWritten(0),
    myMemSize(0)
{
  SetExportFile(theFileName);
  StoreInitialData();
}

const Exchange::Status& Exchange::Result::SetExportFile(const std::string& theFileName)
{
  myStatus = STATUS_OK;
  myResFileName = theFileName;
  // Проверка правильности пути
  if (!myResFileName.empty())
  {
    BoostFS::path aResultPath(myResFileName);
    if (BoostFS::is_directory(aResultPath))
      myStatus = STATUS_WRONGFILENAME;
  }
  return Status();
}

std::string Exchange::Result::StatusDescription() const
{
  return GetStatusDescription(myStatus, myStatusAddInfo);
}

void Exchange::Result::AddResult(const unsigned long& theTimeStep,
                                const std::map<UniqueID, double> theCharges)
{
  if (myData.find(theTimeStep) != myData.end())
    myMemSize += sizeof(unsigned long);
  myMemSize += theCharges.size() * (sizeof(UniqueID) + sizeof(double));

  myData[theTimeStep].push_back(theCharges);
  if (myMemSize > EXCHANGE_MEMORY_THRESHOLD)
    StorePartData();
}

void Exchange::Result::StoreResult()
{
  StorePartData(myData.begin(), myData.end());
}

void Exchange::Result::StoreInitialData()
{
  /** \todo Реализовать сохранение начальных параметров задачи.
   *        Сохранение должно происходить в отдельном потоке для возможности одновремененного
   *        добавления результатов расчёта.
   */
  myInitialWritten = true;
}

void Exchange::Result::StorePartData()
{
  MapTimeData::iterator aFirst = myData.begin();
  MapTimeData::iterator aLast  = myData.end();
  if (myData.size() > 1)
    aLast--;
  StorePartData(aFirst, aLast);
}

void Exchange::Result::StorePartData(MapTimeData::iterator theFirst,
                                    MapTimeData::iterator theLast)
{
  /// \todo Реализовать сохранение части данных в файл
}
