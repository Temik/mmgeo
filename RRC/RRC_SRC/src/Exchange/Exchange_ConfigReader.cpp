/// \file   Exchange_ConfigReader.cpp
/// \author Artem A. Zhidkov
/// \date   03.01.2015
/// \brief  Реализация объекта для чтения конфигурационного файла

#include <Exchange_ConfigReader.h>

#include <RRC_Configure.h>
#include <Function_Factory.h>
#include <Randomizer_Factory.h>

#include <boost/algorithm/string.hpp>
#include <fstream>
#include <sstream>

/** \brief Производит конвертацию конфигурационного файла в набор записей "поле=значение"
 *  \param[in]  theFileName имя конфигурационного файла
 *  \param[out] theConfigVars список переменных и их значений
 *  \return Статус корректности чтения конфигурационного файла
 */
static Exchange::Status ReadConfigFile(const std::string& theFileName,
                                       BoostPO::variables_map& theConfigVars);


Exchange::ConfigReader* Exchange::ConfigReader::mySelf = 0;

Exchange::ConfigReader* Exchange::ConfigReader::GetInstance()
{
  if (!mySelf)
    mySelf = new Exchange::ConfigReader();
  return mySelf;
}

Exchange::ConfigReader::ConfigReader()
  : myStatus(STATUS_OK)
{
}

const Exchange::Status& Exchange::ConfigReader::ProcessConfigFile(const std::string& theFileName)
{
  BoostPO::variables_map aConfigVars;

  myStatus = ReadConfigFile(theFileName, aConfigVars);
  myStatusAddInfo = theFileName;

  // Заполнение размеров сетки
  if (Status() == STATUS_OK)
    SetGridSizes(aConfigVars);

  // Получение размеров частиц и расстояний между ними
  if (Status() == STATUS_OK)
    SetGridParameters(aConfigVars);

  // Получение электрических зарядов частиц
  if (Status() == STATUS_OK)
    SetCharges(aConfigVars);

  // Получение внешнего поля
  if (Status() == STATUS_OK)
    SetExternalField(aConfigVars);

  // Получение имени результирующего файла
  if (Status() == STATUS_OK)
    SetOutputFileName(aConfigVars);

  if (Status() == STATUS_OK)
  {
    myStatusAddInfo.clear();
    myConfigFile = BoostFS::path(theFileName);
  }

  return Status();
}

const Exchange::Status& Exchange::ConfigReader::CopyTemplateTo(const std::string& theOutDir)
{
  myStatus = STATUS_OK;
  myStatusAddInfo.clear();

  // Имя и путь до шаблона конфигурационного файла
  BoostFS::path aTemplateFrom(RRC_DEFAULT_CONFIG_TEMPLATE);

  // Создаём директорию, в которую будет скопирован конфигурационный файл
  BoostFS::path aPathTo(theOutDir);
  BoostFS::path aPathToCheck = aPathTo; // для проверки, существует ли такая директория
  if (!BoostFS::is_directory(aPathTo))
    aPathToCheck.remove_filename(); // оставляем только имя директории
  else // добавляем имя файла для успешного копирования
    aPathTo.append(aTemplateFrom.filename().string());
  if (!BoostFS::exists(aPathToCheck) && !BoostFS::create_directory(aPathToCheck))
  {
    myStatus = STATUS_UNABLECREATEDIR;
    myStatusAddInfo = aPathTo.string();
  }
  else
  {
    BoostSys::error_code aError;
    BoostFS::copy_file(aTemplateFrom, aPathTo, BoostFS::copy_option::overwrite_if_exists, aError);

    if (aError.value())
    {
      myStatus = STATUS_UNABLECOPY;
      myStatusAddInfo = aError.message();
    }
  }

  return Status();
}

std::string Exchange::ConfigReader::StatusDescription() const
{
  return GetStatusDescription(myStatus, myStatusAddInfo);
}



// =================== Auxiliary namespaces ===================================
namespace PARTICLE
{
  static const std::string NAME("PARTICLE");

  static const std::string X_SIZE              (NAME + ".X_SIZE");
  static const std::string Y_SIZE              (NAME + ".Y_SIZE");
  static const std::string Z_SIZE              (NAME + ".Z_SIZE");

  static const std::string CHARGE_SOURCE       (NAME + ".CHARGE_SOURCE");
  static const std::string CHARGE_ARRAY        (NAME + ".CHARGE_ARRAY");
  static const std::string CHARGE_ARRAY_INDEXED(NAME + ".CHARGE_ARRAY_INDEXED");
  static const std::string CHARGE_DISTRIBUTION (NAME + ".CHARGE_DISTRIBUTION");
  static const std::string CHARGE_FUNCTION     (NAME + ".CHARGE_FUNCTION");

  static const std::string SIZES               (NAME + ".SIZES");
  static const std::string DISTANCES           (NAME + ".DISTANCES");
}

namespace CONNECTION
{
  static const std::string NAME("CONNECTION");

  static const std::string RISE_FALL(NAME + ".RISE_FALL");
}

namespace EXTERNAL_FIELD
{
  static const std::string NAME("EXTERNAL_FIELD");

  static const std::string X(NAME + ".X");
  static const std::string Y(NAME + ".Y");
  static const std::string Z(NAME + ".Z");
}

namespace OUTPUT
{
  static const std::string NAME("OUTPUT");

  static const std::string FILE(NAME + ".FILE");
}



// =================== Private methods ========================================
void Exchange::ConfigReader::SetGridSizes(const BoostPO::variables_map& theConfigVars)
{
  if (!theConfigVars.count(PARTICLE::X_SIZE) ||
      !theConfigVars.count(PARTICLE::Y_SIZE) ||
      !theConfigVars.count(PARTICLE::Z_SIZE))
  {
    myStatus = STATUS_WRONGCONFIG;
    return;
  }

  myGridSize[0] = theConfigVars[PARTICLE::X_SIZE].as<unsigned long>();
  myGridSize[1] = theConfigVars[PARTICLE::Y_SIZE].as<unsigned long>();
  myGridSize[2] = theConfigVars[PARTICLE::Z_SIZE].as<unsigned long>();
}

void Exchange::ConfigReader::SetGridParameters(const BoostPO::variables_map& theConfigVars)
{
  if (!theConfigVars.count(PARTICLE::SIZES) ||
      !theConfigVars.count(PARTICLE::DISTANCES))
  {
    myStatus = STATUS_WRONGCONFIG;
    return;
  }

  myParticleSizesDistr =
      Randomizer::Factory::CreateGenerator(theConfigVars[PARTICLE::SIZES].as<std::string>());
  myParticleDistancesDistr =
      Randomizer::Factory::CreateGenerator(theConfigVars[PARTICLE::DISTANCES].as<std::string>());
}

void Exchange::ConfigReader::SetConnectionRiseFall(const BoostPO::variables_map& theConfigVars)
{
  myConnectionProbability = -1.0;
  if (theConfigVars.count(CONNECTION::RISE_FALL))
    myConnectionProbability = theConfigVars[CONNECTION::RISE_FALL].as<double>();

  if (myConnectionProbability < 0.0 || myConnectionProbability > 1.0)
    myStatus = STATUS_WRONGCONFIG;
}

void Exchange::ConfigReader::SetCharges(const BoostPO::variables_map& theConfigVars)
{
  if (!theConfigVars.count(PARTICLE::CHARGE_SOURCE))
  {
    myStatus = STATUS_WRONGCONFIG;
    return;
  }

  std::string aSource = theConfigVars[PARTICLE::CHARGE_SOURCE].as<std::string>();
  boost::to_upper(aSource);
  bool isFile = false;
  if (aSource == std::string("FILE"))
    isFile = true;
  else if (aSource != std::string("VALUE"))
  { // Неверное значение параметра
    myStatus = STATUS_WRONGCONFIG;
    return;
  }

  Function::Type aFuncType;
  std::string aParamName;
  if (theConfigVars.count(PARTICLE::CHARGE_ARRAY))
  {
    aFuncType = Function::FUNC_ARRAY;
    aParamName = PARTICLE::CHARGE_ARRAY;
  }
  else if (theConfigVars.count(PARTICLE::CHARGE_ARRAY_INDEXED))
  {
    aFuncType = Function::FUNC_ARRAY_INDEXED;
    aParamName = PARTICLE::CHARGE_ARRAY_INDEXED;
  }
  else if (theConfigVars.count(PARTICLE::CHARGE_DISTRIBUTION))
  {
    aFuncType = Function::FUNC_DISTRIBUTION;
    aParamName = PARTICLE::CHARGE_DISTRIBUTION;
  }
  else if (theConfigVars.count(PARTICLE::CHARGE_FUNCTION))
  {
    aFuncType = Function::FUNC_ANALYTIC;
    aParamName = PARTICLE::CHARGE_FUNCTION;
  }
  else
  { // Отсутствует необходимый параметр
    myStatus = STATUS_WRONGCONFIG;
    return;
  }

  std::string aParamVal = theConfigVars[aParamName].as<std::string>();
  if (isFile)
  {
    std::ifstream aFileStream(aParamVal.c_str(), std::ifstream::in);
    if (!aFileStream.is_open())
    {
      myStatus = STATUS_WRONGFILENAME;
      myStatusAddInfo = aParamVal;
      return;
    }
    myChargesFunc = Function::Factory::CreateFunction(aFuncType, aFileStream);
  }
  else
  {
    std::istringstream aStrStream(aParamVal, std::istringstream::in);
    myChargesFunc = Function::Factory::CreateFunction(aFuncType, aStrStream);
  }

  if (!myChargesFunc)
    myStatus = STATUS_WRONGCONFIG;
}

void Exchange::ConfigReader::SetExternalField(const BoostPO::variables_map& theConfigVars)
{
  if (!theConfigVars.count(EXTERNAL_FIELD::X) ||
      !theConfigVars.count(EXTERNAL_FIELD::Y) ||
      !theConfigVars.count(EXTERNAL_FIELD::Z))
  {
    myStatus = STATUS_WRONGCONFIG;
    return;
  }

  myExtField[0] = theConfigVars[EXTERNAL_FIELD::X].as<double>();
  myExtField[1] = theConfigVars[EXTERNAL_FIELD::Y].as<double>();
  myExtField[2] = theConfigVars[EXTERNAL_FIELD::Z].as<double>();
}

void Exchange::ConfigReader::SetOutputFileName(const BoostPO::variables_map& theConfigVars)
{
  if (theConfigVars.count(OUTPUT::FILE))
  {
    BoostFS::path aResFile(theConfigVars[OUTPUT::FILE].as<std::string>());
    aResFile = BoostFS::absolute(aResFile, BoostFS::current_path());
    myOutputFile = aResFile.string();
  }
  else
    myStatus = STATUS_WRONGCONFIG;
}



// =================== Auxiliary functions ====================================
Exchange::Status ReadConfigFile(const std::string& theFileName,
                                BoostPO::variables_map& theConfigVars)
{
  BoostPO::options_description anOptions("Config file options");
  anOptions.add_options()
      // ==================================
      //   Параметры частиц
      (
        PARTICLE::X_SIZE.c_str(),
        BoostPO::value< unsigned long >(),
        "Количество частиц вдоль оси X"
      )
      (
        PARTICLE::Y_SIZE.c_str(),
        BoostPO::value< unsigned long >(),
        "Количество частиц вдоль оси Y"
      )
      (
        PARTICLE::Z_SIZE.c_str(),
        BoostPO::value< unsigned long >(),
        "Количество частиц вдоль оси Z"
      )
      (
        PARTICLE::CHARGE_SOURCE.c_str(),
        BoostPO::value< std::string >(),
        "Источник (текущий или внешний файл), из которого будет прочитана информация о зарядах частиц"
      )
      (
        PARTICLE::CHARGE_ARRAY.c_str(),
        BoostPO::value< std::string >(),
        "Заряды частиц заданы в виде массива"
      )
      (
        PARTICLE::CHARGE_ARRAY_INDEXED.c_str(),
        BoostPO::value< std::string >(),
        "Заряды частиц заданы в виде массива квартетов"
      )
      (
        PARTICLE::CHARGE_DISTRIBUTION.c_str(),
        BoostPO::value< std::string >(),
        "Заряды частиц заданы в виде распределения случайной величины"
      )
      (
        PARTICLE::CHARGE_FUNCTION.c_str(),
        BoostPO::value< std::string >(),
        "Заряды заданы в виде функции от индексов частицы"
      )
      (
        PARTICLE::SIZES.c_str(),
        BoostPO::value< std::string >(),
        "Распределение размеров частиц"
      )
      (
        PARTICLE::DISTANCES.c_str(),
        BoostPO::value< std::string >(),
        "Распределение расстояний между частицами"
      )
      // ==================================
      //   Параметры возникновения/разрушения связей
      (
        CONNECTION::RISE_FALL.c_str(),
        BoostPO::value< double >(),
        "Вероятность возникновения/разрушения связей между частицами"
      )
      // ==================================
      //   Параметры внешнего поля
      (
        EXTERNAL_FIELD::X.c_str(),
        BoostPO::value< double >(),
        "Внешнее электрическое поле вдоль оси X"
      )
      (
        EXTERNAL_FIELD::Y.c_str(),
        BoostPO::value< double >(),
        "Внешнее электрическое поле вдоль оси Y"
      )
      (
        EXTERNAL_FIELD::Z.c_str(),
        BoostPO::value< double >(),
        "Внешнее электрическое поле вдоль оси Z"
      )
      // ==================================
      //   Параметры вывода результата
      (
        OUTPUT::FILE.c_str(),
        BoostPO::value< std::string >(),
        "Имя файла для сохранения результата"
      );

  std::ifstream aFile(theFileName.c_str(), std::ifstream::in);
  if (!aFile.is_open())
    return Exchange::STATUS_WRONGFILENAME;
  BoostPO::parsed_options aParsedOptions =
      BoostPO::parse_config_file(aFile, anOptions, false);
  BoostPO::store(aParsedOptions, theConfigVars, true);
  BoostPO::notify(theConfigVars);
  aFile.close();

  return Exchange::STATUS_OK;
}
