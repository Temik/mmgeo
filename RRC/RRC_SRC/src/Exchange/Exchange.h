/// \file   Exchange.h
/// \author Artem A, Zhidkov
/// \date   03.01.2015
/// \brief  Основные определения модуля Exchange

#ifndef _EXCHANGE_H_
#define _EXCHANGE_H_

#if defined WIN32
# if defined Exchange_EXPORTS
#   define EXCHANGE_EXPORT __declspec(dllexport)
# else
#   define EXCHANGE_EXPORT __declspec(dllimport)
# endif
#else
# define EXCHANGE_EXPORT
#endif

#include <string>

#include <boost/program_options.hpp>
namespace BoostPO = boost::program_options;
#include <boost/filesystem.hpp>
namespace BoostFS = boost::filesystem;
namespace BoostSys = boost::system;

namespace Exchange
{
  /// \brief Список статусов, которые могут возвращать объекты модуля Exchange
  enum Status
  {
    STATUS_OK,              ///< Операция проиведена успешно
    STATUS_WRONGFILENAME,   ///< Некорректное имя файла
    STATUS_WRONGCONFIG,     ///< Неверная структура конфигурационного файла
    STATUS_UNABLECREATEDIR, ///< Невозможно создать директорию
    STATUS_UNABLECOPY,      ///< Невозможно скопировать файл
    STATUS_UNKNOWNERROR     ///< Возникла неизвестная ошибка
  };

  /** \brief Генерирует строковое описание возникшей ошибки
   *  \param[in] theStatus  значение ошибки
   *  \param[in] theAddInfo дополнительная информация для более полного описания ошибки
   *  \return Строка с описанием ошибки
   */
  static std::string GetStatusDescription(const Exchange::Status& theStatus,
                                          const std::string& theAddInfo = std::string())
  {
    std::string aResult;

    switch (theStatus)
    {
    case STATUS_OK:
      aResult = "OK";
      break;
    case STATUS_WRONGFILENAME:
      aResult = "Wrong file name";
      break;
    case STATUS_WRONGCONFIG:
      aResult = "Incorrect config file data";
      break;
    case STATUS_UNABLECREATEDIR:
      aResult = "Unable to create directory";
      break;
    case STATUS_UNABLECOPY:
      aResult = "Unable to copy file";
      break;
    default:
      aResult = "Unknown error appears";
      break;
    }

    if (!theAddInfo.empty())
      aResult += ": " + theAddInfo;

    return aResult;
  }
}

#endif // _EXCHANGE_H_
